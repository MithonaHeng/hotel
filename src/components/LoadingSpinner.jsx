// LoadingSpinner.js
import React from 'react';

const LoadingSpinner = () => (
    <div className="d-flex flex-column align-items-center" style={{ height: '100vh' }}>
    <div className="spinner-border text-primary" role="status">
      <span className="visually-hidden">Loading...</span>
    </div>
    <div className="mt-3">Loading...</div>
  </div>
);

export default LoadingSpinner;
