import React from "react";

const InputVert = ({ name, change, value }) => {
  return (
    <div className="mb-3">
      <label className="form-label" htmlFor="room">
        {name}
      </label>
      <input
        type="text"
        className="form-control"
        id={name}
        onChange={change}
        value={value}
      />
    </div>
  );
};

export default InputVert;
