import React from "react";

const SelectValue = ({ title, value, valueChange, defaultValue }) => {
  return (
    <div className="col-md-6 mb-4">
      <label htmlFor="select2Basic" className="form-label">
        {title}
      </label>
      <div className="position-relative">
        <select
          id="select2Basic"
          className="form-select"
          tabIndex="-1"
          aria-hidden="true"
          onChange={valueChange}
          value={defaultValue}
        >
          <option value="">Select Value</option>
          {value.map((v, i) => (
            <option value={v} key={i}>
              {v}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};

export default SelectValue;
