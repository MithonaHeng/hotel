import React from "react";

const PermissionRow = ({ pName, change, isChecked }) => {
  return (
    <tr>
      <td className="text-nowrap fw-medium">{pName}</td>
      <td>
        <div className="d-flex justify-content-end">
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              onChange={change}
              checked={isChecked}
            />
          </div>
        </div>
      </td>
    </tr>
  );
};

export default PermissionRow;
