import React from "react";
const TableRow = ({
  edit,
  Delete,
  children,
  permissionEdit,
  permissionDelete,
  optionalPermission,
  optionalBtn,
}) => {
  return (
    <tr className="col-sm-12 col-md-6">
      {children}
      <td className=" py-3 ">
        <div className="permission">
          {optionalPermission && (
            <button className="btn p-0  border-0">
              <a onClick={optionalBtn}>
                <i className="fa-solid fa-user-lock icon-primary border-0"></i>
              </a>
            </button>
          )}
          {permissionEdit && (
            <button className="btn p-0  border-0">
              <a className="ps-2" onClick={edit}>
                <i className="fa-regular fa-pen-to-square icon-primary border-0"></i>
              </a>
            </button>
          )}
          {permissionDelete && (
            <button className="btn p-0 border-0">
              <a className="ps-2 " onClick={Delete}>
                <i className="fa-solid fa-trash p-0 icon-danger"></i>
              </a>
            </button>
          )}
        </div>
      </td>
    </tr>
  );
};

export default TableRow;
