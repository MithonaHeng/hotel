import React from "react";
import Pagination from "react-bootstrap/Pagination";
const PaginationBasic = ({ prev, next, page, totalPage, onPageChange }) => {
  return (
    <>
      <Pagination>
        <Pagination.Prev onClick={prev} disabled={page === 1} />
        {[...Array(totalPage).keys()].map((number) => (
          <Pagination.Item
            key={number + 1}
            active={number + 1 === page}
            onClick={() => onPageChange(number + 1)}
          >
            {number + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next onClick={next} disabled={page === totalPage} />
      </Pagination>
    </>
  );
};

export default PaginationBasic;
