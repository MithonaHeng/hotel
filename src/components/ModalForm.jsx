import React from "react";
import Modal from "react-bootstrap/Modal";

const ModalForm = ({
  header,
  subtitle,
  children,
  show,
  hide,
  submit,
  submitName,
}) => {
  return (
    <Modal show={show} onHide={hide} centered>
      <div className="modal-content p-3">
        <div className="modal-body">
          <div className="text-center mb-4">
            <h3 className="title">{header}</h3>
            <p>{subtitle}</p>
          </div>
          {children}
          <div className="pt-4 d-flex justify-content-center">
            <button
              type="submit"
              className={
                submitName === "Submit"
                  ? "btn btn-primary me-sm-3 me-1"
                  : submitName === "Update"
                  ? "btn btn-primary me-sm-3 me-1"
                  : "btn btn-danger me-sm-3 me-1"
              }
              onClick={submit}
            >
              {submitName}
            </button>
            <button
              type="reset"
              className="btn btn-label-secondary"
              onClick={hide}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ModalForm;
