
import React from "react";
import { Pagination } from "react-bootstrap";

const PaginationComponent = ({ totalPage, params, handlePage }) => {
  return (
    <Pagination>
      <Pagination.Prev
        onClick={() => handlePage(params.page - 1)}
        disabled={params.page === 1}
      />
      {[...Array(totalPage).keys()].map((number) => (
        <Pagination.Item
          key={number + 1}
          active={number + 1 === params.page}
          onClick={() => handlePage(number + 1)}
        >
          {number + 1}
        </Pagination.Item>
      ))}
      <Pagination.Next
        onClick={() => handlePage(params.page + 1)}
        disabled={params.page === totalPage}
      />
    </Pagination>
  );
};

export default PaginationComponent;
