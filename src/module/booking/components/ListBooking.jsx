import React, { useEffect, useState } from "react";
import { useBooking } from "../core/action.js";
import {useSelector } from "react-redux";
import SortHeader from "./SortComponets.jsx";
import { useNavigate } from "react-router-dom";
import AddBookingModal from "./AddBookingModal.jsx";
import BookingFilter from "./BookingFilter.jsx";
import BookingTable from "./BookingTable.jsx";
import PaginationComponent from "./Pagination.jsx";
import DeleteModal from "./DeleteBooking.jsx";
import LoadingSpinner from "../../../components/LoadingSpinner.jsx";

const ListBooking = () => {
  const {
    getBooking,
    handlePage,
    handleSize,
    handleStatus,
    handlecheckIn,
    handlecheckOut,
    addBookingData,
    deleteBookingData,
    checkInBooking,
    checkOutBooking,
    cancelBooking,
  } = useBooking();

  const { booking, params, totalPage, totals, loading } = useSelector(
    (state) => state.booking
  );

  const [showModal, setShowModal] = useState(false);
  const [deletingBookId, setDeletingBookId] = useState(null);

  useEffect(() => {
    getBooking();
  }, [params, showModal, deletingBookId]);

  const navigate = useNavigate();

  const handleSizeChange = (e) => {
    const newSize = parseInt(e.target.value, 10);
    handleSize(newSize);
  };

  const handleAddBooking = (bookingData) => {
    addBookingData(bookingData);
    setShowModal(false);
  };

  const handleDeleteBooking = (bookId) => {
    setDeletingBookId(bookId);
  };

  const fields = [
    { field: "id", label: "ID" },
    { field: "user", label: "ORDERBY" },
    { field: "status", label: "STATUS" },
    { field: "checkInAt", label: "CHECKIN-AT" },
    { field: "checkOutAt", label: "CHECKOUT-AT" },
  ];

  return (

    <div >
      {loading ? (
        <LoadingSpinner />
      ) : (
        <>
        <div className="rounded bg-light">
          <BookingFilter
            handleStatus={handleStatus}
            handlecheckIn={handlecheckIn}
            handlecheckOut={handlecheckOut}
          />
          <div className="row mx-1 border-bottom py-3 border-top">
            <div className="col-md-6">
              <div className="col-md-2">
                <div className="me-3">
                  <div>
                    <label>
                      <select
                        className="form-select"
                        value={params.size}
                        onChange={handleSizeChange}
                      >
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                      </select>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-5 justify-content-end d-flex ms-4">
              <button
                className="btn btn-primary"
                onClick={() => setShowModal(true)}
              >
                Add Booking
              </button>
            </div>
          </div>
          <table className="table table-hover overflow-y-auto">
            <thead>
              <SortHeader fields={fields} />
            </thead>
            <BookingTable
              booking={booking}
              navigate={navigate}
              handleCheckIn={checkInBooking}
              handleCheckOut={checkOutBooking}
              handleDelete={handleDeleteBooking}
              handelCancel={cancelBooking}
            />
          </table>
          <div className="row ">
            <div className="col-md-5">
              <div>
                <p className=" px-3 text-black fw-semibold">
                  Showing 1 to {params.size} of {totals} entries
                </p>
              </div>
            </div>
            <div className="col-md-6 d-flex justify-content-end">
              <PaginationComponent
                totalPage={totalPage}
                params={params}
                handlePage={handlePage}
              />
            </div>
          </div>
          </div>
        </>
      )}
      <AddBookingModal
        show={showModal}
        handleClose={() => setShowModal(false)}
        handleAddBooking={handleAddBooking}
      />
      <DeleteModal
        deletingUserId={deletingBookId}
        onCancel={() => setDeletingBookId(null)}
        onDelete={deleteBookingData}
      />
    </div>
  );
};

export default ListBooking;
