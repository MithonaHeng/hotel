import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useBooking } from "../core/action";
import { useSelector } from "react-redux";
import { useRoomtypes } from "../../roomType/core/action";
import { reqGetBookingById } from "../core/request";
import SelectValueId from "../../../components/SelectValueId";
const EditBookingModal = ({ show, handleClose, id, bookData }) => {
  const { updateBooking } = useBooking();
  const [updatedBook, setUpdatedBook] = useState({
    userId: "",
    checkIn: "",
    checkOut: "",
    specialRequest: "",
    bookingDetail: [{ roomTypeId: "", adult: "", child: "" }],
  });
  const { getRoomType } = useRoomtypes();
  const { roomtypes } = useSelector((state) => state.roomtypes);
  useEffect(() => {
    setUpdatedBook((prevData) => ({
      ...prevData,
      ...bookData,
    }));
  }, [bookData]);

  useEffect(() => {
    reqGetBookingById(id).then((r) => setUpdatedBook(r.data.data));
    getRoomType();
  }, []);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setUpdatedBook({ ...updatedBook, [name]: value });
  };

  const handleBookingDetailChange = (index, e) => {
    const { name, value } = e.target;
    const bookingDetail = [...updatedBook.bookingDetail];
    bookingDetail[index][name] = value;
    setUpdatedBook({ ...updatedBook, bookingDetail });
  };

  const handleSubmit = () => {
    updateBooking(id, updatedBook);
    handleClose();
  };

  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <div className="modal-content p-3 p-md-5 fw-semibold text-black">
        <Modal.Header
          style={{ borderBottom: "none", justifyContent: "center" }}
        >
          <h4 className="d-flex justify-content-center">
            Edit Booking Information
          </h4>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <div className="row fw-semibold text-black-50">
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicCheckIn">
                  <Form.Label>Check-in Date</Form.Label>
                  <div className="d-flex align-items-center">
                    <label>
                      {new Date(updatedBook.checkIn).toLocaleDateString(
                        "en-US",
                        {
                          year: "numeric",
                          month: "numeric",
                          day: "2-digit",
                        }
                      )}
                    </label>
                    <Form.Control
                      type="date"
                      name="checkIn"
                      value={updatedBook.checkIn}
                      onChange={handleChange}
                      className="w-50 ms-3"
                    />
                  </div>
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicCheckOut">
                  <Form.Label>Check-Out Date</Form.Label>
                  <div className="d-flex align-items-center">
                    <label>
                      {new Date(updatedBook.checkOut).toLocaleDateString(
                        "en-US",
                        {
                          year: "numeric",
                          month: "numeric",
                          day: "2-digit",
                        }
                      )}
                    </label>
                    <Form.Control
                      type="date"
                      name="checkOut"
                      value={updatedBook.checkOut}
                      onChange={handleChange}
                      className="w-50 ms-3"
                    />
                  </div>
                </Form.Group>
              </div>
              <div className="col-12 mb-3">
                <Form.Group controlId="formBasicSpecialRequest">
                  <Form.Label>Special Request</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Enter special request"
                    name="specialRequest"
                    value={updatedBook.specialRequest}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
            </div>
            <div className="col-12 mb-3">
              <SelectValueId
                value={roomtypes}
                title={"Room Type"}
                valueChange={(e) =>
                  setUpdatedBook((prevState) => {
                    const newBookingDetail = [...prevState.bookingDetail];
                    newBookingDetail[0] = {
                      ...newBookingDetail[0],
                      roomTypeId: e.target.value,
                    };
                    return {
                      ...prevState,
                      bookingDetail: newBookingDetail,
                    };
                  })
                }
              />
            </div>

            {updatedBook.bookingDetail.map((detail, index) => (
              <div key={index} className="row fw-semibold text-black-50">
                <div className="col-6 mb-3">
                  <Form.Group controlId={`formBasicChild-${index}`}>
                    <Form.Label>adult</Form.Label>
                    <Form.Control
                      type="input"
                      placeholder="Enter number of children"
                      name="adult"
                      value={detail.adult}
                      onChange={(e) => handleBookingDetailChange(index, e)}
                    />
                  </Form.Group>
                </div>
                <div className="col-6 mb-3">
                  <Form.Group controlId={`formBasicChild-${index}`}>
                    <Form.Label>Child</Form.Label>
                    <Form.Control
                      type="input"
                      placeholder="Enter number of children"
                      name="child"
                      value={detail.child}
                      onChange={(e) => handleBookingDetailChange(index, e)}
                    />
                  </Form.Group>
                </div>
              </div>
            ))}
          </Form>
        </Modal.Body>
        <Modal.Footer
          style={{
            borderTop: "none",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export default EditBookingModal;
