import { useEffect } from 'react';
import Swal from 'sweetalert2';

const DeleteModal = ({ deletingUserId, onCancel, onDelete }) => {
  useEffect(() => {
    if (deletingUserId !== null) {
      Swal.fire({
        title: 'Confirm Deletion',
        text: 'Are you sure you want to delete this user?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#dc3545',
        cancelButtonColor: '#6c757d',
        confirmButtonText: 'Delete',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result.isConfirmed) {
          onDelete(deletingUserId);
        } else {
          onCancel();
        }
      });
    }
  }, [deletingUserId, onCancel, onDelete]);

  return null;
};

export default DeleteModal;
