import React from 'react';
import {useSelector } from 'react-redux';
import { IconButton } from '@mui/material';
import { KeyboardArrowUp, KeyboardArrowDown } from '@mui/icons-material';
import { useBooking } from '../core/action';

const SortHeader = ({ fields }) => {
    const {handleSort} = useBooking();
    const { params } = useSelector(state => state.booking);

    return (
<tr>
            {fields.map(({ field, label }) => (
                <th key={field} >
                    <div className="sort-label" onClick={() => handleSort(field)}>
                        <span>{label}</span>
                        <IconButton
                            size="small"
                            className='text-black p-0'
                            style={{ marginLeft: '5px', opacity: params.sort === field ? 1 : 0.5 }}
                        >
                            {params.sort === field ? (
                                params.order === 'asc' ? <KeyboardArrowUp /> : <KeyboardArrowDown />
                            ) : (
                                <>
                                    <KeyboardArrowUp />
                                </>
                            )}
                        </IconButton>
                    </div>
                </th>
            ))}
            <th >ACTION</th>
        </tr>
    );
};

export default SortHeader;
