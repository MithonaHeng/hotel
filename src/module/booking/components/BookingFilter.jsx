import React from "react";

const BookingFilter = ({
  handleStatus,
  handlecheckIn,
  handlecheckOut
}) => {
  return (
    <div className="d-flex align-items-center row py-4 px-3">
      {/* Status filter */}
      <div className="col-md-4">
        <label className="text-muted mb-2">Select the Status</label>
        <select
          className="form-select"
          onChange={(e) => handleStatus(e.target.value)}
        >
          <option value="">Status</option>
            <option value="CONFIRMED">CONFIRMED</option>
            <option value="CANCELLED">CANCELLED</option>
            <option value="CHECKED_IN">CHECKED_IN</option>
        </select>
      </div>
      {/* Check-in filter */}
      <div className="col-md-4">
        <label className="text-muted mb-2">Select the check-in date</label>
        <input
          id="checkInDate"
          type="date"
          className="form-control"
          placeholder="Select Check-In Date"
          onChange={(e) => handlecheckIn(e.target.value)}
        />
      </div>
      {/* Check-out filter */}
      <div className="col-md-4">
        <label className="text-muted mb-2">Select the check-out date</label>
        <input
          name="dateOfBirth"
          type="date"
          className="form-control"
          onChange={(e) => handlecheckOut(e.target.value)}
        />
      </div>
    </div>
  );
};

export default BookingFilter;
