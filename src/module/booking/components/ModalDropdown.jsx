import React from "react";

const Modal = ({ show, handleClose, handleCheckOut, bookId, navigate, position }) => {
  return (
    <>
      {show && (
        <div className="modal-overlay" style={{ position: 'absolute', top: position.top, left: position.left }}>
          <div className="modal-content btn-label-secondary p-3" style={{ position: 'relative' }}>
            <button
              className="btn btn-close"
              onClick={handleClose}
              style={{ position: 'absolute', top: '10px', right: '10px' }}
            >
              &times;
            </button>
            <button className="btn btn-sm btn-outline-info mt-3" onClick={() => handleCheckOut(bookId)}>
              Check Out
            </button>
            <button className="btn btn-sm btn-outline-primary" onClick={() => navigate(`/payment/${bookId}`)}>
              Payment
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;
