import React, { useState } from "react";
import Modal from "./ModalDropdown";

const BookingTable = ({
  booking,
  navigate,
  handleCheckIn,
  handleCheckOut,
  handleDelete,
  handelCancel,
}) => {
  const [showModal, setShowModal] = useState(false);
  const [selectedBookId, setSelectedBookId] = useState(null);
  const [modalPosition, setModalPosition] = useState({ top: 0, left: 0 });

  const handleToggleModal = (event, id) => {
    const iconRect = event.target.getBoundingClientRect();
    const left = iconRect.left + window.scrollX-150;
    const top = iconRect.top + window.scrollY-50;
    setModalPosition({ top, left });
    setSelectedBookId(id);
    setShowModal(true);
  };
  console.log(booking);

  return (
<tbody>
  {booking.length === 0 ? (
    <tr>
      <td colSpan="6">
        <h5 className="text-center text-secondary">Booking not found !!</h5>
      </td>
    </tr>
  ) : (
    booking.map((book) => (
      <tr key={book.id} style={{ cursor: "pointer" }}>
        <td onClick={() => navigate(`/booking/${book.id}`)}>{book.id}</td>
        <td onClick={() => navigate(`/booking/${book.id}`)}>
          {book.user?.name}
        </td>
        <td onClick={() => navigate(`/booking/${book.id}`)}>
          <span
            className={
              book.status === "CONFIRMED"
                ? "badge bg-label-success rounded-1 px-2 py-2"
                : book.status === "CANCELLED"
                ? "badge bg-label-danger rounded-1 px-3 py-2"
                : book.status === "CHECKED_IN"
                ? "badge bg-label-primary rounded-1 px-2 py-2"
                : book.status === "RESERVED"
                ? "badge bg-label-primary rounded-1 px-2 py-2"
                : book.status === "CHECKED_OUT"
                ? "badge bg-label-info rounded-1 px-2 py-2"
                : book.status === "OCCUPIED"
                ? "badge bg-label-info rounded-1 px-2 py-2"
                : book.status === "MAINTENANCE"
                ? "badge bg-label-info rounded-1 px-2 py-2"
                : ""
            }
          >
            {book.status}
          </span>
        </td>
        <td onClick={() => navigate(`/booking/${book.id}`)}>
          {new Date(book.checkInAt).toLocaleString()}
        </td>
        <td onClick={() => navigate(`/booking/${book.id}`)}>
          {new Date(book.checkOutAt).toLocaleString()}
        </td>
        <td>
          {book.status === "CONFIRMED" && (
            <>
              <button
                className="btn px-0 border-0"
                onClick={() => navigate(`/booking/${book.id}`)}
              >
                <i className="fa-solid fa-circle-info"></i>
              </button>
              <button
                className="btn text-success"
                onClick={() => handleCheckIn(book.id)}
              >
                <i className="fa-solid fa-calendar-check"></i>
              </button>
              <button
                className="btn text-danger px-0 border-0"
                onClick={() => handleDelete(book.id)}
              >
                <i className="fa-solid fa-trash"></i>
              </button>
            </>
          )}
          {book.status === "CHECKED_IN" && (
            <>
              <button
                className="btn px-0 border-0"
                onClick={() => navigate(`/booking/${book.id}`)}
              >
                <i className="fa-solid fa-circle-info"></i>
              </button>
              <button
                className="btn text-primary"
                onClick={(e) => handleToggleModal(e, book.id)}
              >
                <i className="fa-solid fa-right-from-bracket"></i>
              </button>
              <button
                className="btn text-danger px-0 border-0"
                onClick={() => handleDelete(book.id)}
              >
                <i className="fa-solid fa-trash"></i>
              </button>
            </>
          )}
          {book.status === "CANCELLED" && (
            <>
              <button
                className="btn px-0 border-0"
                onClick={() => navigate(`/booking/${book.id}`)}
              >
                <i className="fa-solid fa-circle-info"></i>
              </button>
              <button
                className="btn text-warning"
                onClick={() => handelCancel(book.id)}
              >
                <i className="fa-solid fa-ban"></i>
              </button>
              <button
                className="btn text-danger px-0 border-0"
                onClick={() => handleDelete(book.id)}
              >
                <i className="fa-solid fa-trash"></i>
              </button>
            </>
          )}
          {book.status === "CHECKED_OUT" && (
            <>
              <button
                className="btn px-0 border-0"
                onClick={() => navigate(`/booking/${book.id}`)}
              >
                <i className="fa-solid fa-circle-info"></i>
              </button>
              <button
                className="btn disabled border-0 text-primary"
                onClick={() => handleCheckOut(book.id)}
              >
                <i className="fa-solid fa-right-from-bracket"></i>
              </button>
              <button
                className="btn text-danger px-0 border-0"
                onClick={() => handleDelete(book.id)}
              >
                <i className="fa-solid fa-trash"></i>
              </button>
            </>
          )}
          {showModal && selectedBookId === book.id && (
            <Modal
              show={showModal}
              handleClose={() => setShowModal(false)}
              handleCheckOut={handleCheckOut}
              bookId={book.id}
              navigate={navigate}
              position={modalPosition}
            />
          )}
        </td>
      </tr>
    ))
  )}
</tbody>

  );
};

export default BookingTable;
