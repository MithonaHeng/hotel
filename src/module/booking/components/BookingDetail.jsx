import React, { useEffect, useState } from "react";
import Header from "../../../components/Header.jsx";
import { Link, useParams } from "react-router-dom";
import { useBooking } from "../core/action.js";
import { useSelector } from "react-redux";
import EditBookingModal from "./EditBooingModal.jsx";
import EditPaymentModal from "../../payment/components/EditPayment.jsx"
import LoadingSpinner from "../../../components/LoadingSpinner.jsx";
const BookingDetail = () => {
  const { id } = useParams();
  const { getBookingId, getBooking,getPaymentId,deleteBookingData} = useBooking();
  const [deletingBookId, setDeletingBookId] = useState(null);
  const bookingId = useSelector((state) => state.booking.bookingId);
  const paymentId = useSelector((state) => state.payment.paymentId);
  const { roomtypes } = useSelector((state) => state.roomtypes);
  const { loading } = useSelector((state) => state.booking);
  const handleDeleteBooking = async (bookId) => {
    setDeletingBookId(bookId);
    deleteBookingData(bookId);
  };
  useEffect(() => {
    // getBooking();
    getBookingId(id)
    getPaymentId(id);
  }, [id]);
  console.log(roomtypes);

  const {
    user,
    checkInAt,
    checkOutAt,
    status,
    specialRequest,
    bookingDetail,
    payment,
  } = bookingId;
  const [showEditBookingModal, setShowEditBookingModal] = useState(false);
  const [bookData, setBookData] = useState({});
  const [showEditPaymentModal, setShowEditPaymentModal] = useState(false);
  const [paymentData, setPaymentData] = useState({});
  const handleEditPayment = () => {
    setShowEditPaymentModal(true);
  };

  const handleEdit = () => {
    setBookData({
      userId: user.id,
      checkIn: checkInAt,
      checkOut: checkOutAt,
      specialRequest: specialRequest,
      bookingDetail: bookingDetail,
    });
    setShowEditBookingModal(true);
  };

  const handleCloseEdit = () => {
    setShowEditPaymentModal(false);
    setPaymentData({});
  };

  const handleCloseEditModal = () => {
    setShowEditBookingModal(false);
    setBookData({});
  };

  return (
    <div style={{ margin: "0px 90px" }}>
    <Header title="BOOKING DETAIL" subtitle="Booking Detail" />
    {loading ? (
      <LoadingSpinner />
    ) : (
      <>
        <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
          <div className="d-flex flex-column justify-content-center">
            <h5 className="mb-1 mt-3 text-primary">
              Booking #{id}{" "}
              <span className="badge bg-label-success me-2 ms-2">
                {payment ? payment.paymentStatus : ""}
              </span>{" "}
              <span className="badge bg-label-info">{status}</span>
            </h5>
            <p>
              {new Date(checkInAt).toLocaleDateString("en-US", {
                year: "numeric",
                month: "short",
                day: "numeric",
              })}{" "}
              -{" "}
              {new Date(checkOutAt).toLocaleDateString("en-US", {
                year: "numeric",
                month: "short",
                day: "numeric",
              })}{" "}
            </p>
          </div>
          <div className="d-flex align-content-center flex-wrap gap-2">
            <Link to="/booking">
              <button className="btn btn-primary">Back</button>
            </Link>
            <button className="btn btn-danger" onClick={() => handleDeleteBooking(id)}>Delete Order</button>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="card mb-4">
              <div className="card-header bg-primary text-light d-flex justify-content-between align-items-center">
                <h5 className="card-title m-0 fw-bold">Order Details</h5>
                <button className="btn btn-sm btn-light" onClick={handleEdit}>Edit</button>
              </div>
              <div className="card-body">
                <p>
                  <strong>Booking ID:</strong> {bookingId.id}
                </p>
                <p>
                  <strong>User Name:</strong> {user?.name}
                </p>
                <p>
                  <strong>Check-in Date:</strong>{" "}
                  {new Date(checkInAt).toLocaleDateString()}
                </p>
                <p>
                  <strong>Check-out Date:</strong>{" "}
                  {new Date(checkOutAt).toLocaleDateString()}
                </p>
                <p>
                  <strong>Special Request:</strong> {specialRequest}
                </p>
                <div className="card mb-3">
                  <div className="card-header bg-label-info">
                    <h6 className="card-title m-0">Booking Detail</h6>
                  </div>
                  <div className="card-body">
                    <table className="table">
                      <thead>
                        <tr>
                          <th>Room Type</th>
                          <th>Floor</th>
                          <th>Room Number</th>
                          <th>Adults</th>
                          <th>Children</th>
                        </tr>
                      </thead>
                      <tbody>
                        {bookingDetail?.map((detail, index) => (
                          <tr key={index}>
                            <td>{detail.roomType.title}</td>
                            <td>{detail.room?.floor}</td>
                            <td>{detail.room?.room}</td>
                            <td>{detail.adult}</td>
                            <td>{detail.child}</td>
                          </tr>
                        ))}
                        <tr>
                          <td colSpan="3" className="align-top px-4 py-5">
                            <span>Thanks for your booking</span>
                          </td>
                          <td className="text-end px-4 py-5">
                            <p className="mb-0">Total Payment:</p>
                          </td>
                          <td className="px-4 py-5">
                            <p className="fw-medium mb-0">
                              {payment?.totalPayment}$
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="card mb-4">
              <div className="card-header bg-success text-light d-flex justify-content-between align-items-center">
                <h5 className="card-title m-0 fw-bold">Payment Details</h5>
                <button className="btn btn-sm btn-light" onClick={handleEditPayment}>Edit</button>
              </div>
              <div className="card-body">
                <p>
                  <strong>Payment ID:</strong> {paymentId?.id}
                </p>
                <p>
                  <strong>Total Payment:</strong> {paymentId?.totalPayment}$
                </p>
                <p>
                  <strong>Payment Date:</strong>
                  {new Date(paymentId?.paymentDate).toLocaleDateString()}
                </p>
                <p>
                  <strong>Remark:</strong> {paymentId?.remark}
                </p>
                <p>
                  <strong>Method: </strong>
                  <strong className="badge bg-label-warning fs-6">{paymentId?.paymentMethod}</strong>
                </p>
                <p>
                  <strong>Status: </strong>
                  <strong className="badge bg-label-success fs-6">{payment?.paymentStatus}</strong>
                </p>
              </div>
            </div>
          </div>
        </div>
      </>
    )}
    <EditBookingModal
      show={showEditBookingModal}
      handleClose={handleCloseEditModal}
      id={id}
      bookData={bookData}
    />
    <EditPaymentModal
      show={showEditPaymentModal}
      handleClose={handleCloseEdit}
      id={id}
      paymentData={paymentId}
    />
  </div>
  );
};

export default BookingDetail;
