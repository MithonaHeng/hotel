import React, { useEffect, useState } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useRoomtypes } from "../../roomType/core/action";
const AddBookingModal = ({ show, handleClose, handleAddBooking }) => {
  const { getRoomType } = useRoomtypes();
  const { roomtypes } = useSelector((state) => state.roomtypes);
  const [bookingData, setBookingData] = useState({
    userId: "",
    checkIn: "",
    checkOut: "",
    specialRequest: "",
    bookingDetail: [{ roomTypeId: "", adult: "", child: "" }],
  });
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setBookingData({ ...bookingData, [name]: value });
  };
  const handleBookingDetailChange = (index, e) => {
    const { name, value } = e.target;
    const updatedBookingDetail = [...bookingData.bookingDetail];
    updatedBookingDetail[index][name] = value;
    setBookingData({ ...bookingData, bookingDetail: updatedBookingDetail });
  };

  const handleSubmit = () => {
    handleAddBooking(bookingData);
    handleClose();
    setBookingData({});
  };

  useEffect(() => {
    getRoomType();
  }, []);

  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <div className="modal-content p-3 p-md-5 fw-semibold text-black">
        <Modal.Header
          style={{ borderBottom: "none", justifyContent: "center" }}
        >
          <Modal.Title>
            <h4 className="d-flex justify-content-center">
              Add Booking Information
            </h4>
            <small>Add booking details will receive a privacy audit.</small>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <div className="row fw-semibold text-black-50">
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="checkIn">
                  <Form.Label>Check-in Date</Form.Label>
                  <Form.Control
                    type="date"
                    placeholder="Enter check-in date"
                    name="checkIn"
                    value={bookingData.checkIn}
                    onChange={handleInputChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="checkOut">
                  <Form.Label>Check-out Date</Form.Label>
                  <Form.Control
                    type="date"
                    name="checkOut"
                    value={bookingData.checkOut}
                    onChange={handleInputChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 mb-3">
                <Form.Group controlId="formBasicSpecialRequest">
                  <Form.Label>Special Request</Form.Label>
                  <Form.Control
                    as="select"
                    name="specialRequest"
                    value={bookingData.specialRequest}
                    onChange={handleInputChange}
                  >
                    <option value="">Select Room Type</option>
                    {roomtypes.map((room) => (
                      <option key={room.id} value={room.amenity}>
                        {room.amenity}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </div>
              {bookingData.bookingDetail?.map((detail, index) => (
                <div key={index}>
                  <Form.Group controlId={`roomTypeId-${index}`}>
                    <Form.Label>Room Type</Form.Label>
                    <Form.Control
                      as="select"
                      name="roomTypeId"
                      value={detail.roomTypeId}
                      onChange={(e) => handleBookingDetailChange(index, e)}
                    >
                      <option value="">Select Room Type</option>
                      {roomtypes.map((room) => (
                        <option key={room.id} value={room.id}>
                          {room.title}
                        </option>
                      ))}
                    </Form.Control>
                  </Form.Group>

                  <Form.Group controlId={`adult-${index}`}>
                    <Form.Label>Adults</Form.Label>
                    <Form.Control
                      type="number"
                      name="adult"
                      value={detail.adult}
                      onChange={(e) => handleBookingDetailChange(index, e)}
                    />
                  </Form.Group>
                  <Form.Group controlId={`child-${index}`}>
                    <Form.Label>Children</Form.Label>
                    <Form.Control
                      type="number"
                      name="child"
                      value={detail.child}
                      onChange={(e) => handleBookingDetailChange(index, e)}
                    />
                  </Form.Group>
                </div>
              ))}
            </div>
          </Form>
        </Modal.Body>
        <Modal.Footer
          style={{
            borderTop: "none",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export default AddBookingModal;
