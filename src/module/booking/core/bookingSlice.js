
import { createSlice } from "@reduxjs/toolkit";

const bookingSlice = createSlice({
    name: "booking",
    initialState: {
        booking: [],
        totalPage: 0,
        loading: true,
        error: null,
        user: null,
        params: {
            checkIn:"",
            size: 10,
            page: 1,
            sort: "id",
            order: "asc",
            status: "",
            checkout:""
        },
        bookingId:[], 
        payment:[],
    },
    reducers: {
        setBooking: (state, action) => {
            state.booking = action.payload.data;
            state.totalPage = action.payload.paging.totalPage;
            state.totals = action.payload.paging.totals;
        },
        setParams: (state, action) => {
            state.params = { ...state.params, ...action.payload };
        },
        setPayment: (state, action) => {
            state.payment = action.payload;
        },
        setBookingID: (state, action) => {
            state.bookingId = action.payload.data;
        },
        checkIn: (state, action) => {
            const { bookingId } = action.payload;
            state.checkedIn.push(bookingId);
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
    },
});

export const {
    setBooking,
    setParams,
    setPayment,
    setBookingID,
    checkIn,
    setLoading
} = bookingSlice.actions;

export default bookingSlice.reducer;
