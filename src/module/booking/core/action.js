import { reqGetBooking, reqGetBookingById,reqGetPaymentById,reqUpdatePayment, reqAddBooking ,reqUpdateBooking ,reqCheckInBooking,reqCheckOutBooking,reqDeleteBooking,reqCancelBooking} from "./request";
import {
  setBooking,
  setParams,
  setBookingID,
  setPayment,
  setLoading
} from "./bookingSlice";
import { setPaymentID } from "../../payment/core/paymentSlice";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

const useBooking = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state.booking);

  const getBooking = () => {
    reqGetBooking(params)
      .then((res) => {
        dispatch(setBooking(res.data));
      })
      .catch((error) => {
        console.error("Error fetching users:", error);
      })
      .finally(()=>{
        dispatch(setLoading(false));
      });
  };
  const getBookingId = (id) => {
    reqGetBookingById(id)
      .then((res) => {
        dispatch(setBookingID(res.data))
        getPaymentId(res.data);
      })
      .catch((error) => {
        console.error("Error fetching users:", error);
      })
      .finally(()=>{
        dispatch(setLoading(false));
      });
  };
  const getPaymentId = (bookingData) => {
    const paymentId = bookingData?.data?.payment?.id;
    if (paymentId) {
      reqGetPaymentById(paymentId)
        .then((res) => {
          dispatch(setPaymentID(res.data));
        })
        .catch((paymentError) => {
          console.error("Error fetching payment:", paymentError);
        });
    } else {
      dispatch(setPayment(null));
    }
  };
  const updateBooking  = (id,updateBookingData) =>{
    reqUpdateBooking(`/${id}`,updateBookingData)
            .then((res) => {
                toast.success(res.data.message);
                clearErrors();
            })
            .catch((error) => {
                toast.error("Error: " + error.response.data.message);
            });

  }
  const updatePayment  = (id,updatePaymentData) =>{
    reqUpdatePayment(id,updatePaymentData)
            .then((res) => {
                toast.success(res.data.message);
                getBooking();
                getBookingId();
                clearErrors();
            })
            .catch((error) => {
                toast.error("Error: " + error.response.data.message);
            });

  }
  const addBookingData = (bookingData) => {
    reqAddBooking(bookingData)
      .then((res) => {
        toast.success(res.data.message)
        getBooking();
      })
      .catch((error) => {
        toast.error("Error: " + error.response.data.message);
      });
  };
  const deleteBookingData = (bookId) => {
    reqDeleteBooking(bookId)
        .then((res) => {
            toast.success(res.data.message);
            getBooking();
        })
        .catch((error) => {
            toast.error("Error: " + error.response.data.message);
        });
};

  const handlePage = (page) => dispatch(setParams({ page: page }));

  const handleSort = (field) => {
    const newOrder = params.order === 'asc' ? 'desc' : 'asc';
    dispatch(setParams({ sort: field, order: newOrder }));
};
  

  const handleSize = (size) => {
    dispatch(setParams({ size: size }));
  };
  const handleStatus = (status) => {
    dispatch(setParams({ ...params, status }));
  };
  const handlecheckIn = (checkIn) => {
    dispatch(setParams({ ...params, checkIn }));
  };
  const handlecheckOut = (checkOut) => {
    dispatch(setParams({ ...params, checkOut}));
  };
  const checkInBooking = (bookingId) => {
    reqCheckInBooking(bookingId)
      .then((res) => {
        getBooking();
        toast.success(res.data.message);
      })
      .catch((error) => {
        toast.error("Error: " + error.response.data.message);
      });
  };
  const checkOutBooking = (bookingId) => {
    reqCheckOutBooking(bookingId)
      .then((res) => {
        toast.success(res.data.message);
        getBooking();
      })
      .catch((error) => {
        toast.error("Error: " + error.response.data.message);
      });
  };
  const cancelBooking = (bookingId) => {
    reqCancelBooking(bookingId)
      .then((res) => {
        toast.success(res.data.message);
        getBooking();
      })
      .catch((error) => {
        toast.error("Error: " + error.response.data.message);
      });
  };

  return {
    getBooking,
    getBookingId,
    getPaymentId,
    addBookingData,
    updateBooking,
    deleteBookingData,
    checkInBooking,
    checkOutBooking,
    handleSort,
    handlePage,
    handleSize,
    handleStatus,
    handlecheckIn,
    handlecheckOut,
    updatePayment,
    cancelBooking
  };
};

export { useBooking };
