import axios from "axios";
const reqGetBooking = (params) => {
    return axios.get(`api/booking`, {
        params: params
    });
};
const reqGetBookingById =(id)=>{
    return axios.get(`api/booking/${id}`);
}
const reqGetPaymentById =(paymentId)=>{
    return axios.get(`api/payment/${paymentId}`);
}
const reqAddBooking = (bookingData) => {
    return axios.post('api/booking', bookingData);
}
const reqUpdateBooking = (id,updateBookingData) =>{
    return axios.put(`api/booking${id}`, updateBookingData);
}
const reqUpdatePayment = (id,updatePaymentData) =>{
    return axios.put(`api/payment/${id}`, updatePaymentData);
}
const reqCheckInBooking = (bookingId) => {
    return axios.post(`api/booking/${bookingId}/check-in`);
};
const reqCheckOutBooking = (bookingId) => {
    return axios.post(`api/booking/${bookingId}/check-out`);
};
const reqCancelBooking = (bookingId) => {
    return axios.post(`api/booking/${bookingId}/cancel`);
};
const reqDeleteBooking = (bookId) => {
    return axios.delete(`api/booking/${bookId}`);
};


export {
    reqGetBooking,
    reqGetBookingById,
    reqGetPaymentById,
    reqUpdateBooking,
    reqUpdatePayment,
    reqDeleteBooking,
    reqAddBooking,
    reqCheckInBooking,
    reqCheckOutBooking,
    reqCancelBooking
};