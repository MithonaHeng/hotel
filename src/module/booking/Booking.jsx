import React from "react";
import Header from "../../components/Header.jsx";
import ListBooking from "./components/ListBooking.jsx";
const Booking = () => {
  return (
    <div style={{margin: "0px 90px"}}>
        <Header title="BOOKING" subtitle="List of Booking"/>
        <div style={{margin: "0 0 0 0", height: "75vh", width: "99%"}}>
            <ListBooking/>
        </div>
    </div>

);
};

export default Booking;
