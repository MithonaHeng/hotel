import {useDispatch, useSelector} from "react-redux";
import { reqGetProfile } from './request';
import { setProfile } from './profileSlice';
import {toast} from "react-toastify";

const useProfile = () => {
  const dispatch = useDispatch();

  const getProfile = () => {
    reqGetProfile()
      .then((res) => {
        dispatch(setProfile(res.data));
      })
      .catch((error) => {
        console.error("Error fetching:", error);
      });
  };


  return {  getProfile, 

         };
};

export {useProfile};
