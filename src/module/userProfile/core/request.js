import axios from 'axios';

export const reqGetProfile = () => {
  return axios.get(`api/users/getProfile`, {
  });
};

export const reqResetPass = () => {
  return axios.put(`api/users`);
};

export const reqUpdateProfile = () => {
  return axios.put(`api/users/updateProfile`);
};

export const reqImgProfile = (img) => {
    return axios.post(`api/users/image`, img);
};

export const reqDeleteProfile = (img) => {
    return axios.delete(`api/users/image`, img);
};