import React, { useEffect } from 'react';
import { Modal, ModalTitle, ModalHeader, ModalBody } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useProfile } from './core/action';

const UserProfileModal = ({ show, handleClose }) => {
  const { profile } = useSelector((state) => state.profile);
  const { getProfile } = useProfile();

  useEffect(() => {
    getProfile();
  }, [getProfile]);

  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <div className="modal-content p-3 p-md-5 fw-semibold text-black">
        <ModalHeader style={{ borderBottom: 'none', justifyContent: 'center' }}>
          <ModalTitle>
            <h4 className="d-flex justify-content-center">Profile Details</h4>
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          {profile && (
            <div className="container rounded bg-white mb-5">
              <div className="row">
                <div className="col-md-3 border-right">
                  <div className="d-flex flex-column align-items-center text-center p-3 py-5">
                    <img
                      className="rounded-circle"
                      width="130px"
                      height="130px"
                      src={profile.avatar || "https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"}
                      alt="Profile"
                    />
                    <span className="font-weight-bold mt-2">{profile.name}</span>
                    <div class="d-grid gap-3 mt-3">
                      <div class="p-2 bg-light border"> Bio: {profile.bio}</div>
                    </div>
                  </div>
                </div>
                <div className="col-md-5 border-right">
                  <div className="p-3 py-5">
                    <div className="d-flex justify-content-between align-items-center mb-3">
                      <h4 className="text-right">Profile Settings</h4>
                    </div>
                    <div className="row mt-2">
                      <div className="col-md-6">
                        <label className="labels">Name</label>
                        <p>{profile.name || "Name"}</p>
                      </div>
                      <div className="col-md-6">
                        <label className="labels">Username</label>
                      <p>{profile.username || "Name"}</p>
                      </div>
                    </div>
                    <div className="row mt-1">
                    <div className="col-md-12">
                        <label className="labels">Email</label>
                        <p>{profile.email || "Email"}</p>
                      </div>
                      <div className="col-md-12">
                        <label className="labels">Mobile Number</label>
                        <p>{profile.phone || "Phone number"}</p>
                      </div>
                      <div className="col-md-12">
                        <label className="labels">Address</label>
                        <p>{profile.address || "Address"}</p>
                      </div>
                      <div className="col-md-12">
                        <label className="labels">Role</label>
                        <p>Null</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="py-5">
                    <div className="d-flex justify-content-end">
                      <button className="btn btn-primary w-50 me-2">Reset Password</button>
                      <button className="btn btn-primary w-50">Edit Profile</button>
                    </div>
                    <div className="d-flex justify-content-end mt-2">
                      <button className="btn btn-primary w-50 me-2">Upload Image</button>
                      <button className="btn btn-danger w-50">Delete Image </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </ModalBody>
      </div>
    </Modal>
  );
};

export default UserProfileModal;
