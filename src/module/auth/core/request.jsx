import axios from "axios";
import { getAuth } from "../components/AuthHelper";
const login = async (username, password) => {
  // eslint-disable-next-line no-useless-catch
  try {
    const response = await axios.post(
      "http://13.214.207.172:8002/api/auth/login",
      { username, password },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Basic aG90ZWw6aG90ZWxAMTIz",
        },
      }
    );
    return response.data;
  } catch (error) {
    throw error;
  }
};

const register = async (
  username,
  name,
  password,
  phone,
  email,
  address,
  bio
) => {
  // eslint-disable-next-line no-useless-catch
  try {
    const response = await axios.post(
      "http://13.214.207.172:8002/api/auth/register",
      { username, name, password, phone, email, address, bio },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Basic aG90ZWw6aG90ZWxAMTIz",
        },
      }
    );
    return response.data;
  } catch (error) {
    throw error;
  }
};
const getUser = () => {
  return axios.get("http://13.214.207.172:8002/", {
    headers: {
      Authorization: `Bearer ${getAuth()}`,
    },
  });
};

export { login, register, getUser };
