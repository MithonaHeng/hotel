import React, { useState } from "react";
import { Link, Navigate } from "react-router-dom";
import { register } from "./core/request";
import { toast } from "react-toastify";

const Register = () => {
  const [formData, setFormData] = useState({
    username: "",
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
    phone: "",
    address: "",
    status: "1",
    bio: "",
  });
  const [error, setError] = useState(null);
  const [registered, setRegistered] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (formData.password !== formData.confirmPassword) {
      setError("Please confirm your password.");
      return;
    }
    try {
      await register(
        formData.username,
        formData.name,
        formData.password,
        formData.phone,
        formData.email,
        formData.address,
        formData.status,
        formData.bio
      );
      setRegistered(true);
      toast.success("Registration successful");
    } catch (error) {
      setError("Error registering user");
    }
  };

  const handleChange = (event) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  if (registered) {
    return <Navigate to="/auth/login" />;
  }

  return (
    <div className="container-fluid" style={{ color: "black", height: "auto" }}>
      <style>
        {`
          body {
            margin: 0;
          }
        `}
      </style>
      <div className="row">
        <div className="col-lg-7 col-xl-8 d-none d-lg-flex align-items-center">
          <img
            src="https://demos.themeselection.com/sneat-bootstrap-html-admin-template/assets/img/illustrations/girl-with-laptop-light.png"
            className="img-fluid"
            alt="Login image"
          />
        </div>
        <div className="col-12 col-lg-5 col-xl-4 d-flex align-items-center p-sm-5 p-4" style={{ backgroundColor: "#141B2D" }}>
          <div className="w-100 mx-auto">
            <div className="bg-white rounded p-4">
              <div className="pb-3">
                <div className="d-inline-flex justify-content-center align-items-center" style={{ width: "60px", height: "60px" }}>
                    <img src="/src/img/KiloIT.jpg" alt="kiloit" />
                </div>
                <h4 className="mt-3"><b>Welcome to Kilo IT Hotel !!</b></h4>
              </div>
              <form onSubmit={handleSubmit}>
                <div className="mb-3 row">
                  <div className="col">
                    <label htmlFor="username" className="form-label">User Name</label>
                    <input type="text" className="form-control" id="username" name="username" placeholder="Enter your username" autoComplete="off" autoFocus value={formData.username} onChange={handleChange} />
                  </div>
                  <div className="col">
                    <label htmlFor="name" className="form-label">Name</label>
                    <input type="text" className="form-control" id="name" name="name" placeholder="Enter your name" autoComplete="off" value={formData.name} onChange={handleChange} />
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="email" className="form-label">Email Address</label>
                  <input type="email" className="form-control" id="email" name="email" placeholder="Enter your email" autoComplete="off" value={formData.email} onChange={handleChange} />
                </div>
                <div className="mb-3">
                  <label htmlFor="phone" className="form-label">Phone Number</label>
                  <input type="tel" className="form-control" id="phone" name="phone" placeholder="Enter your phone number" autoComplete="off" value={formData.phone} onChange={handleChange} />
                </div>
                <div className="mb-3 row">
                  <div className="col">
                    <label htmlFor="password" className="form-label">Password</label>
                    <input type="password" className="form-control" id="password" name="password" placeholder="********" autoComplete="off" value={formData.password} onChange={handleChange} />
                  </div>
                  <div className="col">
                    <label htmlFor="confirmPassword" className="form-label">Confirm Password</label>
                    <input type="password" className="form-control" id="confirmPassword" name="confirmPassword" placeholder="********" autoComplete="off" value={formData.confirmPassword} onChange={handleChange} />
                  </div>
                </div>
                <div className="mb-3">
                  <label htmlFor="address" className="form-label">Enter your address</label>
                  <input type="address" className="form-control" id="address" name="address" placeholder="Address" autoComplete="off" value={formData.address} onChange={handleChange} />
                </div>
                <div className="mb-3">
                  <label htmlFor="bio" className="form-label">Bio</label>
                  <textarea className="form-control" id="bio" name="bio" placeholder="Enter your bio" autoComplete="off" value={formData.bio} onChange={handleChange} rows={4} />
                </div>
                {error && <p className="text-danger">{error}</p>}
                <button type="submit" className="btn w-100 mb-3" style={{backgroundColor:"#3E4396", color:"#ffffff"}}>Sign Up</button>
              </form>
              <div className="text-center">
                <div>
                  <Link to="/resetpass" className="text-decoration-none"  style={{color:"#3E4396"}}>Forgot password?</Link>
                </div>
                <div>
                  <Link to="/login" className="text-decoration-none"  style={{color:"#3E4396"}}>Already have an account? Sign In</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;