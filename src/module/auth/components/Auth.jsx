// eslint-disable-next-line no-unused-vars
import React, { createContext, useContext, useEffect, useState } from "react";
import { login } from "../core/request";
import * as authHelper from "./AuthHelper";

const AuthContext = createContext({
  auth: null,
  saveAuth: () => {},
  logout: () => {},
  user: null,
  setUser: () => {},
});

const useAuth = () => {
  return useContext(AuthContext);
};

// eslint-disable-next-line react/prop-types
const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState(authHelper.getAuth());
  const [user, setUser] = useState(null);

  const saveAuth = async (username, password) => {
    // eslint-disable-next-line no-useless-catch
    try {
      const result = await login(username, password);
      if (result && result.status === 200) {
        authHelper.setAuth(result.data.token);
        authHelper.setRoleId(result.data.user.roleId);
        authHelper.setUserInfo(result.data.user.username);
      } else {
        throw new Error("Authentication failed");
      }
    } catch (error) {
      throw error;
    }
  };

  const logout = () => {
    authHelper.removeAuth();
    setAuth(null);
    authHelper.removeRoleId();
    authHelper.removeUserInfo();
  };
  return (
    <AuthContext.Provider
      value={{
        auth,
        saveAuth,
        logout,
        user,
        setUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

// eslint-disable-next-line react-refresh/only-export-components
export { AuthProvider, useAuth };
