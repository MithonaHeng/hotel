import axios from "axios";

const setAuth = (value) => localStorage.setItem("token", value);

const getAuth = () => localStorage.getItem("token");

const setRoleId = (value) => localStorage.setItem("role", value);

const getRoleId = () => localStorage.getItem("role");

const removeAuth = () => localStorage.removeItem("token");

const removeRoleId = () => localStorage.removeItem("role");

const setUserInfo = (value) => localStorage.setItem("userInfo", value);

const getUserInfo = () => localStorage.getItem("userInfo");

const removeUserInfo = () => localStorage.removeItem("userInfo");
const setUpAxios = () => {
  axios.defaults.baseURL = "http://13.214.207.172:8002/";
  axios.defaults.headers = {
    Authorization: `Bearer ${getAuth()}`,
  };
};

export {
  setAuth,
  removeAuth,
  getAuth,
  setUpAxios,
  setRoleId,
  removeRoleId,
  getRoleId,
  setUserInfo,
  getUserInfo,
  removeUserInfo,
};
