/* eslint-disable react/no-unescaped-entities */
// eslint-disable-next-line no-unused-vars
import * as React from "react";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useAuth } from "../auth/components/Auth";
import { toast } from "react-toastify";

const Login = () => {
  const { saveAuth } = useAuth();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await saveAuth(email, password);
      toast.success("Login successfully");
      window.location.reload();
    } catch (error) {
      setError("Incorrect username or password!!");
    }
  };

  return (
    <div className="container-fluid" style={{ color: "black", height: "auto" }}>
      <style>{`body { margin: 0; }`}</style>
      <div className="row">
        <div className="col-lg-7 col-xl-8 d-none d-lg-flex align-items-center">
          <img
            src="https://demos.themeselection.com/sneat-bootstrap-html-admin-template/assets/img/illustrations/boy-with-rocket-light.png"
            className="img-fluid"
            alt="Login image"
          />
        </div>
        <div
          className="col-12 col-lg-5 col-xl-4 d-flex align-items-center p-sm-5 p-4"
          style={{ backgroundColor: "#141B2D" }}
        >
          <div className="w-100 mx-auto">
            <div className="bg-white rounded p-4">
              <div className="pb-3">
                <div
                  className="d-inline-flex justify-content-center align-items-cnter"
                  style={{ width: "50px", height: "50px" }}
                >
                  <img src="\src\img\KiloIT.jpg" alt="kiloit" />
                </div>
                <h4 className="mt-3">
                  <b>Welcome to Kilo IT Hotel !!</b>
                </h4>
              </div>
              <form onSubmit={handleSubmit}>
                <div className="mb-3">
                  <label htmlFor="username" className="form-label">
                    User Name
                  </label>
                  <input
                    type="username"
                    className="form-control"
                    id="username"
                    name="username"
                    placeholder="Enter your username"
                    autoComplete="off"
                    autoFocus
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="password" className="form-label">
                    Password
                  </label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    name="password"
                    placeholder="********"
                    autoComplete="off"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
                <div className="mb-3 form-check">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="remember-me"
                    style={{ backgroundColor: "#3E4396", color: "#ffffff" }}
                  />
                  <label className="form-check-label" htmlFor="remember-me">
                    Remember Me
                  </label>
                </div>
                {error && <p className="text-danger">{error}</p>}
                <button
                  type="submit"
                  className="btn w-100 mb-3"
                  style={{ backgroundColor: "#3E4396", color: "#ffffff" }}
                >
                  Sign In
                </button>
              </form>
              <div className="text-center">
                <div>
                  <Link
                    to="/resetpass"
                    className="text-decoration-none"
                    style={{ color: "#3E4396" }}
                  >
                    Forgot password?
                  </Link>
                </div>
                <div>
                  <Link
                    to="/register"
                    className="text-decoration-none"
                    style={{ color: "#3E4396" }}
                  >
                    Don't have an account? Sign Up
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
