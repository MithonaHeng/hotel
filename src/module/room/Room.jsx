import React, { useState } from "react";
import { Box, useTheme } from "@mui/material";
import Header from "../../components/Header";
import { tokens } from "../../theme";
import { useRooms } from "./core/action";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import TableRow from "../../components/TableRow";
import TableHeader from "../../components/TableHeader";
import { useRole } from "../role/core/action";
import { getRoleId } from "../auth/components/AuthHelper";
import PaginationBasic from "../../components/Pagination";
import AddForm from "./components/AddForm";
import {
  openAdd,
  closeAdd,
  closeEdit,
  openEdit,
  openDelete,
  closeDelete,
} from "./core/reducer";
import EditForm from "./components/EditForm";
import DeleteRoom from "./components/DeleteRoom";
const Room = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const { getRoom, searchRoom, sortRoom, limit, pageChange } = useRooms();
  const { rooms, totalPage, params, totals, addModal, editModal, deleteModal } =
    useSelector((state) => state?.rootReducer?.rooms);
  const { page, sort, order } = params;
  const { permissions } = useSelector((state) => state?.rootReducer?.role);
  const { fetchRoleId } = useRole();
  const roleId = getRoleId();
  const dispatch = useDispatch();
  const [roomId, setRoomId] = useState(null);

  const hasPermission = (permissionId) => {
    const permission = permissions.find((p) => p.id === permissionId);
    return permission ? permission.status : false;
  };

  useEffect(() => {
    getRoom();
  }, [params]);

  useEffect(() => {
    fetchRoleId(roleId);
  }, []);
  // permission
  const canDelete = hasPermission(8);
  const canEdit = hasPermission(7);
  const canCreate = hasPermission(6);
  const canView = hasPermission(5);

  const handlePageChange = (p) => {
    pageChange(p);
  };
  const handleEdit = (id) => {
    setRoomId(id);
    dispatch(openEdit());
  };
  const handleDelete = (id) => {
    setRoomId(id);
    dispatch(openDelete());
  };
  return (
    <>
      <Box m="10px 70px">
        <Header title="ROOM MANAGEMENT" subtitle="List of Rooms" />
        <Box
          m="40px 0 0 0"
          height="75vh"
          width="90%"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .name-column--cell": {
              color: colors.greenAccent[300],
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: colors.blueAccent[700],
              borderBottom: "none",
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: colors.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: colors.blueAccent[700],
            },
            "& .MuiCheckbox-root": {
              color: `${colors.greenAccent[200]} !important`,
            },
          }}
        >
          {/* Add Modal  */}
          {addModal && (
            <AddForm show={addModal} hide={() => dispatch(closeAdd())} />
          )}
          {/* Edit Modal  */}
          {editModal && (
            <EditForm
              show={editModal}
              hide={() => dispatch(closeEdit())}
              id={roomId}
            />
          )}
          {deleteModal && (
            <DeleteRoom
              show={deleteModal}
              hide={() => dispatch(closeDelete())}
              id={roomId}
            />
          )}
          {!canView ? (
            <h2 className=" text-center">UnAuthorize</h2>
          ) : (
            <TableHeader
              nameButton="Add Room"
              setValue={(e) => limit(e.target.value)}
              permissionAdd={canCreate}
              search={(e) => searchRoom(e.target.value)}
              clickAdd={() => dispatch(openAdd())}
            >
              <div
                className="table-reponsive text-nowrap overflow-y-auto"
                style={{ maxHeight: 650 }}
              >
                <table className="table table-hover text-center">
                  {/* table title  */}
                  <thead className=" text-uppercase sticky-top">
                    <tr>
                      <th>ID</th>
                      <th onClick={() => sortRoom("id")}>
                        Room ID{" "}
                        {sort === "id" && (
                          <span>{order === "asc" ? "↑" : "↓"}</span>
                        )}
                      </th>
                      <th onClick={() => sortRoom("floor")}>
                        Floor{" "}
                        {sort === "floor" && (
                          <span>{order === "asc" ? "↑" : "↓"}</span>
                        )}
                      </th>
                      <th onClick={() => sortRoom("status")}>
                        Status{" "}
                        {sort === "status" && (
                          <span>{order === "asc" ? "↑" : "↓"}</span>
                        )}
                      </th>
                      <th onClick={() => sortRoom("createdBy")}>
                        Create By{" "}
                        {sort === "createdBy" && (
                          <span>{order === "asc" ? "↑" : "↓"}</span>
                        )}
                      </th>
                      <th>Create Date</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody className="table-border-bottom-0 text-center bg-light overflow-y-auto">
                    {rooms === null ? (
                      <h2 className=" text-center text-dark">Room not found</h2>
                    ) : (
                      rooms.map((room) => {
                        return (
                          <TableRow
                            key={room.id}
                            Delete={() => handleDelete(room.id)}
                            permissionDelete={canDelete}
                            permissionEdit={canEdit}
                            edit={() => handleEdit(room.id)}
                          >
                            <td className=" py-3">{room.id}</td>
                            <td className=" py-3">{room.room}</td>
                            <td className=" py-3">{room.floor}</td>
                            <td className=" py-3">
                              <span
                                className={
                                  room.status == "AVAILABLE"
                                    ? "bagde bg-label-success rounded-1 px-2"
                                    : room.status == "RESERVED"
                                    ? "bagde bg-label-danger rounded-1 px-2"
                                    : "bagde bg-label-warning rounded-1 px-2"
                                }
                              >
                                {room.status}
                              </span>
                            </td>
                            <td className=" py-3">{room.createdBy?.name}</td>
                            <td className=" py-3">
                              {new Date(room.createdAt).toLocaleString("en-US")}
                            </td>
                          </TableRow>
                        );
                      })
                    )}
                  </tbody>
                </table>
              </div>
              <div className="row mx-2">
                <div className="col-sm-12 col-md-6">
                  <div
                    className="dataTables_info"
                    id="DataTables_Table_0_info"
                    aria-live="polite"
                  >
                    {rooms?.length == 0
                      ? "0 room"
                      : `Showing 1 to ${rooms?.length} rooms of ${totals} entries`}
                  </div>
                </div>
                <div className="col-sm-12 col-md-6 d-flex justify-content-end">
                  <PaginationBasic
                    prev={() => pageChange(page - 1)}
                    totalPage={totalPage}
                    page={page}
                    next={() => pageChange(page + 1)}
                    onPageChange={handlePageChange}
                  />
                </div>
              </div>
            </TableHeader>
          )}
        </Box>
      </Box>
    </>
  );
};

export default Room;
