import { useDispatch, useSelector } from "react-redux";
import {
  reqCreateRoom,
  reqRoom,
  reqDeleteRoom,
  reqUpdateRoom,
} from "./request";
import { setRooms, setParams, setTotalPage, setTotal } from "./reducer";
import { toast } from "react-toastify";
const useRooms = () => {
  const dispatch = useDispatch();
  const { params } = useSelector((state) => state?.rootReducer?.rooms);
  const { sort } = params;
  const getRoom = () => {
    reqRoom(params).then((res) => {
      dispatch(setRooms(res.data.data));
      dispatch(setTotalPage(res.data.paging.totalPage));
      dispatch(setTotal(res.data.paging.totals));
    });
  };
  const postRoom = (data) => {
    reqCreateRoom(data)
      .then((res) => {
        dispatch(setRooms(res.data.data));
        getRoom();
      })
      .catch(() => {
        toast.error("Something went wrong");
      });
  };
  const updateRoom = (id, data) => {
    reqUpdateRoom(`/${id}`, data)
      .then(() => {
        getRoom();
      })
      .catch(() => {
        toast.error("Something went wrong");
      });
  };
  const deleteRoom = (id) => {
    reqDeleteRoom(id)
      .then(() => {
        getRoom();
        toast.success("Room deleted");
      })
      .catch(() => {
        toast.error("Something went wrong");
      });
  };

  const sortRoom = (field) => {
    if (sort === field) {
      dispatch(setParams({ order: sort === field ? "desc" : "asc" }));
    } else {
      dispatch(setParams({ sort: field, order: "asc" }));
    }
  };

  const pageChange = (page) => dispatch(setParams({ page: page }));

  const limit = (limit) => {
    dispatch(setParams({ size: limit }));
  };

  const searchRoom = (text) => {
    dispatch(setParams({ page: 1, query: text }));
  };

  return {
    getRoom,
    postRoom,
    updateRoom,
    deleteRoom,
    searchRoom,
    sortRoom,
    pageChange,
    limit,
  };
};

export { useRooms };
