import { createSlice } from "@reduxjs/toolkit";

const RoomApi = createSlice({
  name: "roomApi",
  initialState: {
    rooms: [],
    addModal: false,
    editModal: false,
    deleteModal: false,
    totalPage: 0,
    totals: 0,
    params: {
      size: 10,
      page: 1,
      query: "",
      sort: "",
      order: "asc",
    },
  },
  reducers: {
    setRooms: (state, action) => {
      state.rooms = action.payload;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setTotalPage: (state, action) => {
      state.totalPage = action.payload;
    },
    setTotal: (state, action) => {
      state.totals = action.payload;
    },
    openEdit: (state) => {
      state.editModal = true;
    },
    closeEdit: (state) => {
      state.editModal = false;
    },
    openAdd: (state) => {
      state.addModal = true;
    },
    closeAdd: (state) => {
      state.addModal = false;
    },
    openDelete: (state) => {
      state.deleteModal = true;
    },
    closeDelete: (state) => {
      state.deleteModal = false;
    },
  },
});
export const {
  setRooms,
  setParams,
  setTotalPage,
  setTotal,
  openAdd,
  closeAdd,
  openEdit,
  closeEdit,
  openDelete,
  closeDelete,
} = RoomApi.actions;
export default RoomApi.reducer;
