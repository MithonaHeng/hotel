import axios from "axios";

const reqRoom = (params) => {
  return axios.get("api/v1.0.0/room", {
    params: params,
  });
};

const reqUpdateRoom = (id, data) => {
  return axios.put("api/v1.0.0/room" + id, data);
};

const reqCreateRoom = (data) => {
  return axios.post("api/v1.0.0/room", data);
};

const reqDeleteRoom = (id) => {
  return axios.delete("api/v1.0.0/room" + `/${id}`);
};

const reqRoomById = (id) => {
  return axios.get("api/v1.0.0/room" + `/${id}`);
};

export { reqCreateRoom, reqDeleteRoom, reqRoom, reqRoomById, reqUpdateRoom };
