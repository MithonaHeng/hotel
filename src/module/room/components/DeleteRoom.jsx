import React from "react";
import ModalForm from "../../../components/ModalForm";
import { useRooms } from "../core/action";
import { useDispatch } from "react-redux";
import { closeDelete } from "../core/reducer";

const DeleteRoom = ({ show, hide, id }) => {
  const { deleteRoom } = useRooms();
  const dispatch = useDispatch();
  const handleSubmit = () => {
    deleteRoom(id);
    dispatch(closeDelete());
  };
  return (
    <ModalForm
      show={show}
      hide={hide}
      submitName={"Delete"}
      submit={handleSubmit}
      header={"Delete Room"}
      subtitle={"Do you want delete this room?"}
    ></ModalForm>
  );
};

export default DeleteRoom;
