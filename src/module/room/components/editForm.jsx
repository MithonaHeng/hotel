import React, { useEffect, useState } from "react";
import ModalForm from "../../../components/ModalForm";
import InputVert from "../../../components/InputVert";
import { reqRoomById } from "../core/request";
import SelectValue from "../../../components/SelectValue";
import SelectValueId from "../../../components/SelectValueId";
import { useRoomtypes } from "../../roomType/core/action";
import { useDispatch, useSelector } from "react-redux";
import { useRooms } from "../core/action";
import { closeEdit } from "../core/reducer";

const EditForm = ({ show, hide, id }) => {
  const [updatedData, setUpdatedData] = useState({
    room: "",
    floor: "",
    status: "",
    room_type_id: "",
  });
  const select = ["AVAILABLE", "OCCUPIED", "RESERVED", "MAINTENANCE"];
  const { getRoomType } = useRoomtypes();
  const { roomtypes } = useSelector((state) => state.roomtypes);
  const { updateRoom } = useRooms();
  const dispatch = useDispatch();

  const handleSubmit = () => {
    updateRoom(id, updatedData);
    dispatch(closeEdit());
  };
  useEffect(() => {
    reqRoomById(id).then((r) => setUpdatedData(r.data.data));
    getRoomType();
  }, []);

  return (
    <ModalForm
      show={show}
      hide={hide}
      submit={handleSubmit}
      submitName={"Submit"}
    >
      <InputVert
        name={"Room"}
        change={(e) => setUpdatedData({ ...updatedData, room: e.target.value })}
        value={updatedData.room}
      />
      <InputVert
        name={"Floor"}
        change={(e) =>
          setUpdatedData({ ...updatedData, floor: e.target.value })
        }
        value={updatedData.floor}
      />
      <div className="row">
        <SelectValue
          title={"Status"}
          value={select}
          defaultValue={updatedData.status}
          valueChange={(e) =>
            setUpdatedData({ ...updatedData, status: e.target.value })
          }
        />
        <SelectValueId
          value={roomtypes}
          title={"Room Type"}
          valueChange={(e) =>
            setUpdatedData({ ...updatedData, room_type_id: e.target.value })
          }
        />
      </div>
    </ModalForm>
  );
};

export default EditForm;
