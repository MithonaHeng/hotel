import React, { useEffect, useState } from "react";
import { useRooms } from "../core/action";
import ModalForm from "../../../components/ModalForm";
import SelectValue from "../../../components/SelectValue";
import { useDispatch, useSelector } from "react-redux";
import SelectValueId from "../../../components/SelectValueId";
import { useRoomtypes } from "../../roomType/core/action";
import { closeAdd } from "../core/reducer";
import InputVert from "../../../components/InputVert";

const AddForm = ({ show, hide }) => {
  const [room, setRoom] = useState("");
  const [floor, setFloor] = useState("");
  const [status, setStatus] = useState("");
  const [roomType, setRoomType] = useState("");
  const { roomtypes } = useSelector((state) => state.roomtypes);
  const { getRoomType } = useRoomtypes();
  const { postRoom } = useRooms();
  const dispatch = useDispatch();

  const select = ["AVAILABLE", "OCCUPIED", "RESERVED", "MAINTENANCE"];

  const data = {
    room: room,
    floor: floor,
    status: status,
    room_type_id: roomType,
  };
  useEffect(() => {
    getRoomType();
  }, []);

  const handleSubmit = () => {
    postRoom(data);
    dispatch(closeAdd());
  };
  return (
    <ModalForm
      header={"Add Room"}
      subtitle={"Add more rooms"}
      show={show}
      hide={hide}
      submit={() => handleSubmit()}
      submitName={"Submit"}
    >
      <div className="form-add">
        <form>
          <InputVert name={"Room"} change={(e) => setRoom(e.target.value)} />
          <InputVert name={"Floor"} change={(e) => setFloor(e.target.value)} />
          <div className="row">
            <SelectValue
              title={"Status"}
              value={select}
              valueChange={(e) => setStatus(e.target.value)}
            />
            <SelectValueId
              value={roomtypes}
              title={"Room Type"}
              valueChange={(e) => setRoomType(e.target.value)}
            />
          </div>
        </form>
      </div>
    </ModalForm>
  );
};

export default AddForm;
