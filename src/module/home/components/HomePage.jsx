import React from 'react';
import { useAuth } from '../../auth/components/Auth';

const HomePage = () => {
    const { logout } = useAuth();

    const handleLogout = () => {
        logout();
    };

    return (
        <div>
            <button onClick={handleLogout}>
                Logout
            </button>
        </div>
    );
};

export default HomePage;
