import React, { useEffect, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useBooking } from "../../booking/core/action";
import { useSelector } from "react-redux";
const EditPaymentModal = ({ show, handleClose, id, paymentData }) => {
  const { updatePayment } = useBooking();
  const paymentId = useSelector((state) => state.payment.paymentId);
  const [updatedPayment, setUpdatedPayment] = useState({
    PaymantId: "",
    totalPayment: "",
    paymentStatus: "",
    paymentMethod: "",
    paymentDate: "",
    remark: "",
  });

  useEffect(() => {
    setUpdatedPayment({ ...paymentData });
  }, [paymentData]);
  console.log(updatedPayment);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUpdatedPayment({ ...updatedPayment, [name]: value });
  };
  const handleSubmit = () => {
    updatePayment(paymentId.id, updatedPayment);
    handleClose();
  };

  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <div className="modal-content p-3 p-md-5 fw-semibold text-black">
        <Modal.Header
          style={{ borderBottom: "none", justifyContent: "center" }}
        >
          <h4 className="d-flex justify-content-center">
            Edit Payment Information
          </h4>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <div className="row fw-semibold text-black-50">
              <div className="col-12 mb-3">
                <Form.Group controlId="formBasicTotalPayment">
                  <Form.Label>Total Payment</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter total payment"
                    name="totalPayment"
                    value={updatedPayment.totalPayment}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              {/* Add additional payment fields */}
              <Form.Group controlId="formBasicPaymentStatus">
                <Form.Label>Payment Status</Form.Label>
                <Form.Select
                  value={updatedPayment.paymentStatus}
                  onChange={handleChange}
                  name="paymentStatus"
                >
                  <option value="">Select Payment Status</option>
                  <option value="PAID">PAID</option>
                  <option value="CANCELLED">CANCELLED</option>
                  <option value="PENDING">PENDING</option>
                </Form.Select>
              </Form.Group>
              <div className="col-12 mb-3">
                <Form.Group controlId="formBasicPaymentMethod">
                  <Form.Label>Payment Method</Form.Label>
                  <Form.Select
                    placeholder="Enter payment method"
                    name="paymentMethod"
                    value={updatedPayment.paymentMethod}
                    onChange={handleChange}
                  >
                  <option value="">Select Payment Status</option>
                  <option value="CASH">CASH</option>
                  <option value="BANK_TRANSFER">BANK_TRANSFER</option>
                  </Form.Select>
                </Form.Group>
              </div>
              <div className="col-12 mb-3">
                <Form.Group controlId="formBasicPaymentDate">
                  <Form.Label>Payment Date</Form.Label>
                  <Form.Control
                    type="date"
                    name="paymentDate"
                    value={updatedPayment.paymentDate}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 mb-3">
                <Form.Group controlId="formBasicRemark">
                  <Form.Label>Remark</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    placeholder="Enter remark"
                    name="remark"
                    value={updatedPayment.remark}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
            </div>
          </Form>
        </Modal.Body>
        <Modal.Footer
          style={{
            borderTop: "none",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export default EditPaymentModal;
