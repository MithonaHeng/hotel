import React, { useEffect, useState } from "react";
import Header from "../../components/Header.jsx";
import { Link, useParams } from "react-router-dom";
import { useBooking } from "../booking/core/action.js";
import { useSelector } from "react-redux";
import EditPaymentModal from "./components/EditPayment.jsx";
import LoadingSpinner from "../../components/LoadingSpinner.jsx";

const BookingDetail = () => {
  const { id } = useParams();
  const { getBookingId, getBooking, getPaymentId } = useBooking();
  const bookingId = useSelector((state) => state.booking.bookingId);
  const paymentId = useSelector((state) => state.payment.paymentId);
  const {loading}  = useSelector((state) => state.booking);

  useEffect(() => {
    getBooking();
    getBookingId(id);
    getPaymentId(id);
  }, [id, getBooking, getBookingId, getPaymentId]);

  const { user, checkInAt, checkOutAt, status, payment } = bookingId;

  const [showEditPaymentModal, setShowEditPaymentModal] = useState(false);

  const handleEditPayment = () => {
    setShowEditPaymentModal(true);
  };

  const handleCloseEdit = () => {
    setShowEditPaymentModal(false);
  };

  return (
    <div style={{ margin: "0px 90px" }}>
      <Header title="PAYMENT DETAIL" subtitle="Payment Detail" />
      {loading ? (
        <LoadingSpinner />
      ) : (
        <>
          <div className="d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center mb-3">
            <div className="d-flex flex-column justify-content-center">
              <h5 className="mb-1 mt-3">
                Booking #{id}{" "}
                <span className="badge bg-label-success me-2 ms-2">
                  {payment ? payment.paymentStatus : ""}
                </span>{" "}
                <span className="badge bg-label-info">{status}</span>
              </h5>
              <p>
                {new Date(checkInAt).toLocaleDateString("en-US", {
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                })}{" "}
                -{" "}
                {new Date(checkOutAt).toLocaleDateString("en-US", {
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                })}
              </p>
            </div>
            <div className="d-flex align-content-center flex-wrap gap-2">
              <Link to="/booking">
                <button className="btn btn-primary">Back</button>
              </Link>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="card mb-4">
                <div className="card-header bg-success text-light d-flex justify-content-between align-items-center">
                  <h5 className="card-title m-0 fw-bold">Payment Details</h5>
                  <button className="btn btn-sm btn-light" onClick={handleEditPayment}>
                    Edit
                  </button>
                </div>
                <div className="card-body">
                  <p><strong>Payment ID:</strong> {paymentId?.id}</p>
                  <p><strong>Total Payment:</strong> {paymentId?.totalPayment}$</p>
                  <p><strong>Payment Date:</strong> {new Date(paymentId?.paymentDate).toLocaleDateString()}</p>
                  <p><strong>Remark:</strong> {paymentId?.remark}</p>
                  <p><strong>Method: </strong>
                    <strong className="badge bg-label-warning fs-6">{paymentId?.paymentMethod}</strong>
                  </p>
                  <p><strong>Status: </strong>
                    <strong className="badge bg-label-success fs-6">{payment?.paymentStatus}</strong>
                  </p>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card mb-4">
                <div className="card-header bg-primary text-light d-flex justify-content-between align-items-center">
                  <h5 className="card-title m-0 fw-bold">User Details</h5>
                </div>
                <div className="card-body">
                  <p><strong>UserID:</strong> {user?.id}</p>
                  <p><strong>UserName:</strong> {user?.name}</p>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
      <EditPaymentModal
        show={showEditPaymentModal}
        handleClose={handleCloseEdit}
        id={id}
        paymentData={paymentId}
      />
    </div>
  );
};

export default BookingDetail;
