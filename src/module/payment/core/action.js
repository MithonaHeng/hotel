import { useDispatch, useSelector } from "react-redux";
import { setPaymentID, setLoading } from "./paymentSlice";
import { reqGetPaymentById, reqUpdatePayment } from "./request";
import { toast } from "react-toastify";
const usePayment = () => {
  const dispatch = useDispatch();
  const getPaymentId = (id) => {
    reqGetPaymentById(id)
      .then((res) => {
        dispatch(setPaymentID(res.data));
        console.log(res);
      })
      .catch((error) => {
        console.error("Error fetching users:", error);
      })
      .finally(() => {
        dispatch(setLoading(false));
      });
  };

  const updatePayment = (id, updatePaymentData) => {
    reqUpdatePayment(id, updatePaymentData)
      .then((res) => {
        toast.success(res.data.message);
        getPaymentId();
        clearErrors();
      })
      .catch((error) => {
        toast.error("Error: " + error.response.data.message);
      });
  };
  return {
    getPaymentId,
    updatePayment,
  };
};
export { usePayment };
