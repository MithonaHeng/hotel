import axios from "axios";
const reqGetPaymentById =(id)=>{
    return axios.get(`api/payment/${id}`);
}
const reqUpdatePayment = (id,updatePaymentData) =>{
    return axios.put(`api/payment/${id}`, updatePaymentData);
}

export {
    reqGetPaymentById,
    reqUpdatePayment
}