import { createSlice } from "@reduxjs/toolkit";
const paymentSlice = createSlice({
    name:"payment",
    initialState:{
        payment:[],
        loading: true,
        paymentId:[]

    },
    reducers:{
        setPaymentID: (state, action) => {
            state.paymentId = action.payload.data;
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        },

    }
})

export const {setPaymentID,setLoading} = paymentSlice.actions;

export default paymentSlice.reducer;