import React, { useContext, useState, useEffect } from "react";
import { Box, IconButton, useTheme } from "@mui/material";
import { tokens, ColorModeContext } from "../../../../theme";
import LightModeOutlinedIcon from "@mui/icons-material/LightModeOutlined";
import DarkModeOutlinedIcon from "@mui/icons-material/DarkModeOutlined";
import NotificationsOutlinedIcon from "@mui/icons-material/NotificationsOutlined";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import UserProfileModal from "../../../userProfile/UserProfileModal";
import { useAuth } from "../../../auth/components/Auth";
import { useSelector } from 'react-redux';
import { useProfile } from "../../../userProfile/core/action";

const Topbar = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const colorMode = useContext(ColorModeContext);
  const { user, logout } = useAuth();
  const { profile } = useSelector((state) => state.profile);
  const { getProfile } = useProfile();

  useEffect(() => {
    getProfile();
  }, []);

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleIcon = () => {
    setIsModalOpen(true);
  };

  const handleClose = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Box display="flex" justifyContent="space-between" p={2}>
        {/* SEARCH BAR */}
        <Box
          display="flex"
          backgroundColor={colors.primary[400]}
          borderRadius="3px"
        ></Box>
        {/* ICONS */}
        <Box display="flex">
          <IconButton onClick={colorMode.toggleColorMode}>
            {theme.palette.mode === "dark" ? (
              <DarkModeOutlinedIcon />
            ) : (
              <LightModeOutlinedIcon />
            )}
          </IconButton>
          <IconButton>
            <NotificationsOutlinedIcon />
          </IconButton>
          <IconButton>
            <SettingsOutlinedIcon />
          </IconButton>
          <IconButton onClick={handleIcon}>
            {profile && (
              <div className="avatar avatar-online">
                <img
                  src={profile.avatar || "https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"}
                  alt=""
                  style={{ width: "50px", height: "50px", borderRadius: "50%" }}
                />
              </div>
            )}
          </IconButton>


          <button
            className="btn btn-danger mx-3 px-2 text-center"
            onClick={logout}
            style={{
              borderRadius: "5px",
              fontWeight: "bold",
              fontSize: "16px",
              padding: "10px 20px",
            }}
          >
            <i className="fa-solid fa-right-from-bracket me-2"></i>
            Logout
          </button>
        </Box>
      </Box>
      <UserProfileModal
        show={isModalOpen}
        handleClose={handleClose}  // Ensure handleClose function is correctly used
      />

    </>
  );
};

export default Topbar;
