import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Pagination } from "react-bootstrap";
import { useRoomtypes } from './core/action';
import { useNavigate } from 'react-router-dom';
import EditModal from './components/EditRoom';
import DeleteModal from './components/DeleteRoom';
import AddRoom from './components/CreateRoom';
import Header from "../../components/Header.jsx";

const RoomType = () => {
  const navigate = useNavigate(); 
  const [ roomData, setRoomData ] = useState(null);
  const [showAddModal, setShowAddModal] = useState(false);
  const [selectedAmenity, setSelectedAmenity] = useState('');
  const { roomtypes, params, totalPage, totals } = useSelector((state) => state.roomtypes);
  const [ deletingRoomType, setDeletingRoomType ] = useState(null);
  const {
      getRoomType,
      createRoom,
      editRoomType,
      deleteRoomType,
      handleSearch,
      handlePage,
      handleSize,
      handleAmenity,
      handleBed
  } = useRoomtypes();

  useEffect(() => {
      getRoomType();
  }, [params]);      

  const handleEdit = (roomtype, roomData) => {
    setRoomData(roomData);
  };

  const handleEditSubmit = (updatedRoomData) => {
    editRoomType(updatedRoomData)
      setRoomData(null);
      handleClose();
  };
  
  const handleDelete = (roomId) => {
    setDeletingRoomType(roomId);
  };

  const confirmDelete = () => {
    deleteRoomType(deletingRoomType)
        setDeletingRoomType(null);
        handleClose();
  };

  const handleSizeChange = (e) => {
    const newSize = parseInt(e.target.value, 10);
    handleSize(newSize);
  };
  
  const getUniqueAmenities = () => {
    const allAmenities = roomtypes.flatMap(roomtype => roomtype.amenity.split(',').map(amenity => amenity.trim()));
    return [...new Set(allAmenities)];
  };

  const handleAmenityChange = (amenity) => {
    setSelectedAmenity(amenity);
    handleAmenity(amenity);
  };
  
  return (
    <div style={{margin: "0px 90px"}}>
      <Header title="ROOMTYPE MANAGEMENT" subtitle="List of RoomType"/>
      <div style={{margin: "0 0 0 0", height: "75vh", width: "99%"}}>
        <div className=" bg-light rounded">
          <div className="d-flex align-items-center row py-4 px-3">
            <div className="col-md-2">
              <label className="text-muted mb-2">Find Room by bed</label>
              <input
                type="number"
                className="form-control"
                placeholder="Enter number of beds"
                onChange={(e) => handleBed(e.target.value)}
              />
            </div>

            <div className="col-md-2">
              <label className="text-muted mb-2">Select Amenity</label>
              <div>
                  <select
                      className="form-select text-capitalize"
                      value={selectedAmenity}
                      onChange={(e) => handleAmenityChange(e.target.value)}
                  >
                      <option value="">All Amenities</option>
                      {getUniqueAmenities().map((amenity, index) => (
                          <option key={index} value={amenity}>
                              {amenity}
                          </option>
                      ))}
                  </select>
              </div>
          </div>

          </div>
        <div className="row mx-1 border-bottom py-3 border-top">
          <div className="col-md-6">
            <div className="col-md-2">
              <div className="me-3">
                <div>
                  <label>
                    <select
                      className="form-select"
                      value={params.size}
                      onChange={handleSizeChange}
                    >
                      <option value="5">5</option>
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                    </select>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 justify-content-end d-flex">
                <div className="col-md-5">
                  <div className="d-flex align-items-center justify-content-end">
                    <label className="d-flex align-items-center">
                      Search
                      <input
                        type="search"
                        className="form-control"
                        placeholder="Search"
                        aria-label="Search"
                        onChange={(e) => handleSearch(e.target.value)}
                      />
                    </label>
                  </div>
                </div>
                <div className="col-md-2 btn-group flex-wrap">
              
                    <button
                      className="btn ms-2 btn-primary mb-3 mb-md-0"
                      onClick={() => setShowAddModal(true)}
                    >
                      Add Room
                    </button>
                </div>
              </div>
        </div>
        <table className="table">
          <thead>
            <tr className=" text-uppercase">
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              <th>Bed</th>
              <th>Price</th>
              <th>Amenities</th>
              <th>By</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {roomtypes.map((roomtype) => (
              <tr key={roomtype.id}>
                <td>{roomtype.id}</td>
                <td>{roomtype.title}</td>
                <td>{roomtype.description}</td>
                <td>{roomtype.bed}</td>
                <td>$ {roomtype.price}</td>
                <td>{roomtype.amenity}</td>
                <td>{roomtype.createdBy?.name}</td>
                <td className="py-3" style={{ verticalAlign: 'middle' }}>
                      <button
                        className="btn btn-icon px-0 border-0"
                        key={roomtype.id}
                        onClick={() => navigate(`/roomtypedetail/${roomtype.id}`)}
                        style={{ cursor: "pointer" }}
                      >
                        <i class="fa-solid fa-circle-info"></i>
                      </button>
                      <button
                        className="btn btn-icon border-0"
                        onClick={() => handleEdit(roomtype.id, roomtype)}
                      >
                        <i className="far fa-pen-to-square text-primary"></i>
                      </button>
                      <button
                        className="btn btn-icon delete-record px-0 border-0"
                        onClick={() => handleDelete(roomtype.id)}
                      >
                        <i className="fas fa-trash text-danger"></i>
                      </button>
                      </td>

              </tr>
            ))}
          </tbody>
        </table>

        <div className="row ">
          <div className="col-md-5">
            <div>
              <p className=" px-3 text-black fw-semibold">
                Showing 1 to {params.size} of {totals} entries
              </p>
            </div>
          </div>
          <div className="col-md-6 d-flex justify-content-end">
            <Pagination>
              <Pagination.Prev
                onClick={() => handlePage(params.page - 1)}
                disabled={params.page === 1}
              />
              {[...Array(totalPage).keys()].map((number) => (
                <Pagination.Item
                  key={number + 1}
                  active={number + 1 === params.page}
                  onClick={() => handlePage(number + 1)}
                >
                  {number + 1}
                </Pagination.Item>
              ))}
              <Pagination.Next
                onClick={() => handlePage(params.page + 1)}
                disabled={params.page === totalPage}
              />
            </Pagination>
          </div>
        </div>
        <AddRoom
            show={showAddModal}
            handleClose={() => setShowAddModal(false)}
        />
        <EditModal
          show={roomData != null}
          handleClose={() => setRoomData(null)}
          roomData={roomData}
          onSubmit={handleEditSubmit}
        />

        <DeleteModal
          open={deletingRoomType != null}
          handleClose={() => setDeletingRoomType(null)}
          onConfirm={confirmDelete}
        />
      </div>
      
      </div>
    </div>
  );  
};

export default RoomType;
