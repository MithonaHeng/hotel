import React, { useEffect, useState } from 'react';
import Header from '../../../components/Header';
import { Carousel } from 'react-bootstrap';
import { useParams } from "react-router-dom";
import { useRoomtypes } from "../core/action";
import { useSelector } from "react-redux";
import AddImage from './addImage';

const RoomTypeDetail = () => {
    const { id } = useParams();
    const { getRoomTypeID } = useRoomtypes();
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        getRoomTypeID(id);
    }, [id,showModal]);

    const roomtypeID = useSelector((state) => state.roomtypes.roomtypeID);

    return (
        <div className="container">
            <Header title="ROOMTYPE DETAIL" subtitle={`RoomType ID: ${id}`} />
            <div className="row mt-5">
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title"><b>DESCRIPTION</b></h5>
                            <p className="card-text">{roomtypeID.description}</p>
                        </div>
                    </div>
                    <div className="card mt-3">
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item"><b>Name:</b> {roomtypeID.title}</li>
                            <li className="list-group-item"><b>Sub-Name:</b> {roomtypeID.subTitle}</li>
                            <li className="list-group-item"><b>Amenities:</b> {roomtypeID.amenity}</li>
                            <li className="list-group-item"><b>Price:</b> ${roomtypeID.price}</li>
                        </ul>
                    </div>
                    <div className="card mt-3">
                        <ul className="list-group list-group-fglush">
                            <li className="list-group-item">
                                <b>Created at:</b> {new Date(roomtypeID.createdAt).toLocaleString()}
                            </li>
                            <li className="list-group-item"><b>Created by:</b> {roomtypeID.createdBy?.name}</li>
                        </ul>
                    </div>
                </div>
                <div className="col-md-6">
                    {roomtypeID.image && roomtypeID.image.length > 0 ? (
                        <Carousel>
                            {roomtypeID.image.map(image => (
                                <Carousel.Item key={image.id}>
                                    <img
                                        className="d-block w-100"
                                        style={{ height: "400px" }}
                                        src={image.url}
                                        alt={`Slide ${image.id}`}
                                        onError={(e) => {
                                            e.target.onerror = null;
                                            e.target.src = 'placeholder.jpg';
                                            e.target.alt = 'No Image';
                                            e.target.style.objectFit = 'contain';
                                        }}
                                    />
                                </Carousel.Item>
                            ))}
                        </Carousel>
                    ) : (
                        <div
                            className="d-flex justify-content-center align-items-center border"
                            style={{ height: "400px", border: "1px solid #ccc" }}
                        >
                            <p className="text-center">NO IMAGE DATA</p>
                        </div>
                    )}

                    <div className="mt-3 d-flex justify-content-end">
                        <button className='btn btn-primary' onClick={() => setShowModal(true)}>
                            Add Image
                        </button>
                    </div>

                    <div className="card mt-3">
                        <div className="card-body">
                            <li className="list-group-item"><b>Bed:</b> {roomtypeID.bed}</li>
                            <li className="list-group-item"><b>Children:</b> {roomtypeID.children}</li>
                            <li className="list-group-item"><b>Adult:</b> {roomtypeID.adult}</li>
                        </div>
                    </div>
                </div>
            </div>
            <AddImage 
                show={showModal} 
                setShowModal={setShowModal} 
            />
        </div>      
    );
};

export default RoomTypeDetail;
