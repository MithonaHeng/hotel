import React, { useState } from "react";
const Pagination = ({ total, onChangePage }) => {
  const [active, setActive] = useState(1);

  let items = [];
  for (let number = 1; number <= total; number++) {
    items.push(
      <li
        className={
          active === number
            ? "paginate_button page-item active"
            : "paginate_button page-item"
        }
        key={number}
      >
        <a
          onClick={() => {
            setActive(number);
            onChangePage && onChangePage(number);
          }}
          aria-controls="DataTables_Table_0"
          role="link"
          aria-current="page"
          data-dt-idx="0"
          tabIndex="0"
          className="page-link"
        >
          {number}
        </a>
      </li>
    );
  }
  return <ul className="pagination ">{items}</ul>;
};

export default Pagination;
