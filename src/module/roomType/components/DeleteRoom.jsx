import React from 'react';
import { Button, Modal } from "react-bootstrap";

const DeleteModal = ({ open, handleClose, onConfirm }) => {
  return (
    <Modal show={open} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title style={{ color: "red" }}>
          Confirm Deletion
        </Modal.Title>
      </Modal.Header>
      <Modal.Body style={{ color: "green" }}>
        Are you sure you want to delete this Room Type?
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Cancel
        </Button>
        <Button variant="danger" onClick={onConfirm}>
          Delete
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DeleteModal;