import React, { useState, useEffect } from 'react';
import { Button, Form, Modal, ModalTitle } from "react-bootstrap";

  const initialData = {
    title: '',
    sub_title: '',
    description: '',
    bed: '',
    adult: '',
    children: '',
    price: '',
    amenity: '',
  }

  const editModal = ({show, handleClose, roomData, onSubmit}) => {
    const [updatedRoom, setUpdatedRoom] = useState({
        ...initialData
    });

  useEffect(() => {
    setUpdatedRoom((prevData) => ({
      ...prevData,
      ...roomData
    }));
  }, [roomData]);

  const handleChange = (e) => {
    const {name, value} = e.target;
    setUpdatedRoom({...updatedRoom, [name]: value});
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmit(updatedRoom);
    setUpdatedRoom({
        ...initialData
    });
  };

  return (
    <Modal show={show} onHide={handleClose} size="lg">
        <div className=" modal-content p-3 p-md-5 fw-semibold text-black">
        <Modal.Header style={{  borderBottom: 'none', justifyContent: 'center' }}>
        <ModalTitle>
          <h4 className=" d-flex justify-content-center">Edit Room Type</h4>
        </ModalTitle>
      </Modal.Header>
      <Modal.Body>
        <Form>
        <div className=" row fw-semibold text-black-50">
        <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicUsername">
            <Form.Label >Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter name"
              name="title"
              value={updatedRoom.title}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
            <Form.Group controlId="formBasicEmail">
              <Form.Label >Sub-Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter sub-title"
                name="sub_title"
                value={updatedRoom.sub_title}
                onChange={handleChange}
              />
            </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
            <Form.Group controlId="formBasicprice">
              <Form.Label >Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                name="price"
                value={updatedRoom.price}
                onChange={handleChange}
              />
            </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicPassword">
            <Form.Label >Bed</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter number of bed"
              name="bed"
              value={updatedRoom.bed}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicAddress">
            <Form.Label >Adult</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter number of adult"
              name="adult"
              value={updatedRoom.adult}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicchildren">
            <Form.Label >children</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter number children"
              name="children"
              value={updatedRoom.children}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <Form.Group controlId="formBasicDes">
            <Form.Label style={{ color: "green" }}>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="Enter description"
              name="description"
              value={updatedRoom.description}
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group controlId="formBasicamenity">
            <Form.Label >amenity</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter amenities"
              name="amenity"
              value={updatedRoom.amenity}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
        </Form>
      </Modal.Body>
      <Modal.Footer style={{  borderTop: 'none', display:'flex' , justifyContent:'center'}}>
      <Button variant="primary" onClick={handleSubmit}>
          Submit
        </Button>
        <Button variant="danger" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
      </div>
    </Modal>
  );
}
export default editModal;
