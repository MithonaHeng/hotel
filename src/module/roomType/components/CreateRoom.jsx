import React, { useState } from 'react';
import { useRoomtypes } from '../core/action';
import { Button, Form, Modal, ModalTitle } from "react-bootstrap";

const initialData = {
  title: '',
  sub_title: '',
  description: '',
  bed: '',
  adult: '',
  children: '',
  price: '',
  amenity: '',
}

const CreateRoom = ({ show, handleClose }) => {
  const { createRoom } = useRoomtypes();

  const [formData, setFormData] = useState({
    ...initialData
  });

  const handleChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await createRoom(formData);
      handleClose();
      clearForm();
      toast.success('Room added successfully');
    } catch (error) {
      toast.error('Something went wrong. Please try again.');
    }
  };

  const clearForm = () => {
    setFormData({
      ...initialData
    });
  };

  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <div className=" modal-content p-3 p-md-5 fw-semibold text-black">
        <Modal.Header
          style={{ borderBottom: "none", justifyContent: "center" }}
        >
          <ModalTitle>
            <h4 className=" d-flex justify-content-center">
              Add RoomType Information
            </h4>
          </ModalTitle>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <div className=" row fw-semibold text-black-50">
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter name"
                    name="title"
                    value={formData.title}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicSub-name">
                  <Form.Label>Sub-title</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter sub-title"
                    name="sub_title"
                    value={formData.sub_title}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12  mb-3">
              <Form.Group controlId="formBasicAmenity">
                  <Form.Label>Amenities:</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter amenity"
                    name="amenity"
                    value={formData.amenity}
                    onChange={handleChange}
                  />
                  </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicBed">
                  <Form.Label>Bed</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter bed"
                    name="bed"
                    value={formData.bed}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicAdult">
                  <Form.Label>Adult</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter adult"
                    name="adult"
                    value={formData.adult}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicChildren">
                  <Form.Label>Children</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter children"
                    name="children"
                    value={formData.children}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicPrice">
                  <Form.Label>Price</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter price"
                    name="price"
                    value={formData.price}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <Form.Group controlId="formBasicDescription">
                <Form.Label style={{ color: "green" }}>Bio</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="Enter Description"
                  name="description"
                  value={formData.description}
                  onChange={handleChange}
                />
              </Form.Group>
            </div>
          </Form>
        </Modal.Body>
        <Modal.Footer
          style={{
            borderTop: "none",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export default CreateRoom;
