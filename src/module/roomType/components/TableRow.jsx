import React from "react";

const TableRow = ({
  edit,
  Delete,
  children,
  permissionEdit,
  permissionDelete,
}) => {
  return (
    <tr className="col-sm-12 col-md-6">
      {children}
      <td className=" py-3">
        <div className=" permission">
          {permissionEdit && (
            <button className="btn p-0">
              <a className="dropdown-item" onClick={edit}>
                <i className="fa-regular fa-pen-to-square ps-2 me-2"></i>
              </a>
            </button>
          )}
          {permissionDelete && (
            <button className="btn p-0">
              <a className="dropdown-item" onClick={Delete}>
                <i className="fa-solid fa-trash ps-2 me-2"></i>
              </a>
            </button>
          )}
        </div>
      </td>
    </tr>
  );
};

export default TableRow;
