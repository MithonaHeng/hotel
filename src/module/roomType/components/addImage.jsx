import React, { useState, useRef } from 'react';
import { useRoomtypes } from '../core/action';
import { useParams } from 'react-router-dom';

const AddImage = ({ show, setShowModal }) => {
    const [imageFile, setImageFile] = useState(null);
    const {addRoomtypeImage , getRoomType} = useRoomtypes();
    const fileInput = useRef();
    const id = useParams().id;

    const handleSubmit = () => {
        if (imageFile) {
            const formData = new FormData();
            formData.append('images', imageFile);
            try {
                addRoomtypeImage(id , formData)
                setImageFile(null);
                setShowModal(false)
            } catch (error) {
                console.log(error);
            }
        }
    };
    
    const handleFileChange = (e) => {
        setImageFile(e.target.files[0]);
    };

    return (
        <div className={`modal ${show ? 'show' : ''}`} style={{ display: show ? 'block' : 'none' }}>
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Add Image</h5>
                    </div>
                    <div className="modal-body">
                        <form>
                            <div className="mb-3">
                                <label htmlFor="images" className="form-label">Image</label>
                                <input type="file" className="form-control" id="images" onChange={handleFileChange} accept="image/*" required ref={fileInput} />                            </div>

                        </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" onClick={() => setShowModal(false)}>Close</button>
                        <button type="button" className="btn btn-primary" onClick={handleSubmit}>Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AddImage;
