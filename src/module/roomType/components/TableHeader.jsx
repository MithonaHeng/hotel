import React from "react";

const TableHeader = ({
  nameButton,
  children,
  setValue,
  permissionAdd,
  search,
  handleSearch,
}) => {
  return (
    <div className="card">
      <div className="card-data-table table-responsive">
        <div className=" row mx-2 py-3">
          <div className="col-sm-12 col-md-4 col-lg-6">
            <div className="dataTables_length" id="DataTables_Table_0_length">
              <label>
                <select
                  name="DataTables_Table_0_length"
                  aria-controls="DataTables_Table_0"
                  className="form-select"
                  onChange={setValue}
                >
                  <option value="10">10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="100">100</option>
                </select>
              </label>
            </div>
          </div>
          <div className="col-sm-12 col-md-8 col-lg-6">
            <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center align-items-center flex-sm-nowrap flex-wrap me-1">
              <div className="me-3">
                <div
                  id="DataTables_Table_0_filter"
                  className="dataTables_filter"
                >
                  <label className=" d-flex align-items-center">
                    Search
                    <input
                      type="search"
                      className="form-control ms-2"
                      placeholder="Search ..."
                      aria-controls="DataTables_Table_0"
                      onChange={search}
                    />
                    <a
                      onClick={handleSearch}
                    >
                    </a>
                  </label>
                </div>
              </div>
              <div className="dt-buttons btn-group flex-wrap">
                {permissionAdd && (
                  <button
                    className="btn add-new btn-primary mb-3 mb-md-0"
                    tabIndex="0"
                    aria-controls="DataTables_Table_0"
                    type="button"
                    data-bs-toggle="modal"
                    data-bs-target="#addPermissionModal"
                  >
                    <span>{nameButton}</span>
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      {children}
    </div>
  );
};

export default TableHeader;
