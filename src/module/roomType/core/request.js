import axios from 'axios';

// List room type
export const reqShowRoomType = (params) => {
  return axios.get(`api/v1.0.0/room-type`, {
    params: params
  });
};

export const reqGetRoomTypeID = (id) => {
  return axios.get(`api/v1.0.0/room-type/${id}`);
};

// To add room type
export const reqAddRoomType = async (data) => {
  try {
    const response = await axios.post('api/v1.0.0/room-type', data);
    return response.data;
  } catch (error) {
    throw error;
  }
};

// To add image room type
export const reqAddImg = async (id, img) => {
  return await axios.post(`api/v1.0.0/room-type/${id}/image`, img);
};
// To delete room type
export const reqDeleteRoomType = async (roomId) => {
  return axios.delete(`api/v1.0.0/room-type/${roomId}`);
};

// To edit room type
export const reqEditRoomType = (editedRoom) => {
  return axios.put(`api/v1.0.0/room-type/${editedRoom.id}`, editedRoom);
};

