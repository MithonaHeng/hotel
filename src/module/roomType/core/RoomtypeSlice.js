import { createSlice } from '@reduxjs/toolkit';

const roomtypeSlice = createSlice({
  name: 'roomtype',
  initialState: {
    roomtypes: [],
    totalPage: 0,
    room: null,
    params: {
      query: "",
      sort: "id",
      order: "asc",
      page: 1,
      size: 5,
      bed: 0,
      amenity: "",
    },
    roomtypeID: [],
  },

  reducers: {
    setRoom: (state, action) => {
      state.roomtypes = action.payload.data;
      state.totalPage = action.payload.paging.totalPage;
      state.totals = action.payload.paging.totals;
    },

    addRoomtype: (state, action) => {
      state.roomtypes.push(action.payload);
    },

    deleteUser: (state, action) => {
      const roomIdToDelete = action.payload;
      state.roomtypes = state.roomtypes.filter((room) => room.id !== roomIdToDelete);
    },

    editRoomtype: (state, action) => {
      const {id, updatedRoom} = action.payload;
      state.roomtypes = state.roomtypes.map(room =>
        room.id === id ? {...room, ...updatedRoom} : room
      );
    },

    setParams: (state, action) => {
      state.params = {...state.params, ...action.payload};
    },

    setRoomTypeDetail: (state, action) => {
      state.roomtypeID = action.payload.data;
    },

    setRoomtypeImage: (state, action) => {
      state.roomtypeID = action.payload;
    },

  },
});

export const {  addRoomtype, 
                deleteRoomtype, 
                editRoomtype, 
                addRoomtypeImage, 
                setRoom, 
                setRoomTypeDetail, 
                setParams,
                setRoomtypeImage 
              } = roomtypeSlice.actions;
export default roomtypeSlice.reducer;
