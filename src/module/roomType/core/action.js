import {useDispatch, useSelector} from "react-redux";
import { reqAddRoomType, reqShowRoomType, reqEditRoomType, reqDeleteRoomType, reqGetRoomTypeID, reqAddImg } from './request';
import { addRoomtype, setRoom, editRoomtype, setParams, setRoomTypeDetail, setRoomtypeImage } from './RoomtypeSlice';
import {toast} from "react-toastify";

const useRoomtypes = () => {
  const dispatch = useDispatch();
  const {params} = useSelector((state) => state.roomtypes);
  const {sort} = params;

  const getRoomType = () => {
    reqShowRoomType(params)
      .then((res) => {
        dispatch(setRoom(res.data));
      })
      .catch((error) => {
        console.error("Error fetching:", error);
      });
  };

  const getRoomTypeID = (id) => {
    reqGetRoomTypeID(id)
        .then((res) => {
            dispatch(setRoomTypeDetail(res.data));
        })
        .catch((error) => {
            console.error("Error fetching data:", error);
        });
  };

  const createRoom = async (data) => {
    try {
      const res = await reqAddRoomType(data);
      dispatch(addRoomtype(res.data));
      toast.success("Room Type added successfully");
      getRoomType();
      return res.data;
    } catch (error) {
      toast.error("Failed to add Room Type !!");
      throw error;
    }
  };

  const editRoomType = (editedRoom) => {
      reqEditRoomType(editedRoom)
        .then((res) => {
            toast.success("Room Type updated successfully");
            getRoomType();
        })
        .catch((error) => {
            toast.error("Failed to edit Room Type !!");
        });
  };

  const deleteRoomType = (roomId) => {
      reqDeleteRoomType(roomId)
      .then(() => {
        toast.success("Room Type Deleted successfully");
        getRoomType();
      })
      .catch((error) => {
        toast.error("Failed to delete Room Type !!");
        throw error;
      });
  };

  const handlePage = (page) => dispatch(setParams({page: page}))

  const handleSort = (field) => {
    if (sort.field === field) {
      dispatch(setParams({ field, order: sort.order === "asc" ? "desc" : "asc" }));
    } else {
      dispatch(setParams({ field, order: "asc" }));
    }
  }; 

  const handleSize = (size) => {
    dispatch(setParams({size:size}));
  };

  const handleAmenity = (amenity) => {
    if (amenity) {
      dispatch(setParams({amenity: amenity }));
    } else {
      dispatch(setParams({amenity: "" }));
    }
  };

  const handleBed = (bed) => {
    if (bed) {
      dispatch(setParams({bed: bed }));
    } else {
      dispatch(setParams({bed: 0 }));
    }
  };
  
  const handleSearch = (text) => {
      dispatch(setParams({page: 1, query: text}));
  };

  const addRoomtypeImage = ( id , img ) => {
    reqAddImg(id, img)
      .then(() => {
        getRoomTypeID(id)
        toast.success("Image added to room type successfully");
      })
      .catch((error) => {
        toast.error("Failed to add image to room type");
        console.log(error)
      });
  };
  

  return { createRoom, 
            getRoomType, 
            getRoomTypeID,
            editRoomType, 
            deleteRoomType, 
            handlePage, 
            handleSearch, 
            handleSort,
            handleSize,
            handleAmenity,
            handleBed,
            addRoomtypeImage };
};

export {useRoomtypes};
