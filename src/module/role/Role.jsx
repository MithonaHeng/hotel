import { Box, useTheme } from "@mui/material";
import React, { useEffect, useState } from "react";
import Header from "../../components/Header";
import { tokens } from "../../theme";
import TableHeader from "../../components/TableHeader";
import { useRole } from "./core/action";
import { useDispatch, useSelector } from "react-redux";
import TableRow from "../../components/TableRow";
import PaginationBasic from "../../components/Pagination";
import AddRole from "./components/AddRole";
import {
  closeAddRole,
  closeDeleteRole,
  closeEditRole,
  closePermission,
  openAddRole,
  openDeleteRole,
  openEditRole,
  openPermission,
} from "./core/reducer";
import EditRole from "./components/EditRole";
import DeleteRole from "./components/DeleteRole";
import UpdatePermission from "./components/UpdatePermission";
import { getRoleId } from "../auth/components/AuthHelper";

const Role = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [roleId, setRoleId] = useState(null);
  const {
    role,
    totals,
    totalPage,
    params,
    addRoleModal,
    deleteRoleModal,
    editRoleModal,
    permissionModal,
    permissions,
  } = useSelector((state) => state?.rootReducer?.role);
  const { sort, page, order } = params;
  const { fetchRole, limit, pageChange, searchRole, sortRole, fetchRoleId } =
    useRole();
  const dispatch = useDispatch();
  const userId = getRoleId();
  // function
  const handlePageChange = (p) => {
    pageChange(p);
  };
  const handleDelete = (id) => {
    setRoleId(id);
    dispatch(openDeleteRole());
  };
  const handleEdit = (id) => {
    setRoleId(id);
    dispatch(openEditRole());
  };
  const handlePermission = (id) => {
    setRoleId(id);
    dispatch(openPermission());
  };
  // permission
  const hasPermission = (permissionId) => {
    const permission = permissions.find((p) => p.id === permissionId);
    return permission ? permission.status : false;
  };

  const canView = hasPermission(1);
  const canCreate = hasPermission(2);
  const canEdit = hasPermission(3);
  const canDelete = hasPermission(4);

  useEffect(() => {
    fetchRole();
    fetchRoleId(userId);
  }, [params]);
  return (
    <>
      <Box m="10px 70px">
        <Header title="ROLE MANAGEMENT" subtitle="List of Role" />
        <Box
          m="40px 0 0 0"
          height="75vh"
          width="90%"
          sx={{
            "& .MuiDataGrid-root": {
              border: "none",
            },
            "& .name-column--cell": {
              color: colors.greenAccent[300],
            },
            "& .MuiDataGrid-cell": {
              borderBottom: "none",
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: colors.blueAccent[700],
              borderBottom: "none",
            },
            "& .MuiDataGrid-virtualScroller": {
              backgroundColor: colors.primary[400],
            },
            "& .MuiDataGrid-footerContainer": {
              borderTop: "none",
              backgroundColor: colors.blueAccent[700],
            },
            "& .MuiCheckbox-root": {
              color: `${colors.greenAccent[200]} !important`,
            },
          }}
        >
          {/* add role  */}
          {addRoleModal && (
            <AddRole
              show={addRoleModal}
              hide={() => dispatch(closeAddRole())}
            />
          )}
          {/* edit role  */}
          {editRoleModal && (
            <EditRole
              show={editRoleModal}
              hide={() => dispatch(closeEditRole())}
              id={roleId}
            />
          )}
          {/* delete role  */}
          {deleteRoleModal && (
            <DeleteRole
              show={deleteRoleModal}
              hide={() => dispatch(closeDeleteRole())}
              id={roleId}
            />
          )}
          {permissionModal && (
            <UpdatePermission
              show={permissionModal}
              hide={() => dispatch(closePermission())}
              id={roleId}
            />
          )}
          {!canView ? (
            <h2 className=" text-center">UnAuthorize</h2>
          ) : (
            <TableHeader
              clickAdd={() => dispatch(openAddRole())}
              permissionAdd={canCreate}
              nameButton={"Add Role"}
              setValue={(e) => limit(e.target.value)}
              search={(e) => searchRole(e.target.value)}
            >
              <div
                className="table-reponsive text-nowrap overflow-y-auto w-100"
                style={{ maxHeight: 650 }}
              >
                <table className="table table-hover text-center">
                  {/* table title  */}
                  <thead className=" text-uppercase sticky-top">
                    <tr>
                      <th>No</th>
                      <th onClick={() => sortRole("id")}>
                        Role ID{" "}
                        {sort === "id" && (
                          <span>{order === "asc" ? "↑" : "↓"}</span>
                        )}
                      </th>
                      <th onClick={() => sortRole("name")}>
                        Role Name{" "}
                        {sort === "name" && (
                          <span>{order === "asc" ? "↑" : "↓"}</span>
                        )}
                      </th>
                      <th onClick={() => sortRole("code")}>
                        Role Code{" "}
                        {sort === "code" && (
                          <span>{order === "asc" ? "↑" : "↓"}</span>
                        )}
                      </th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody className="table-border-bottom-0 text-center bg-light overflow-y-auto">
                    {role.map((r, i) => (
                      <TableRow
                        permissionDelete={canDelete}
                        permissionEdit={canEdit}
                        optionalPermission={canEdit}
                        key={i}
                        Delete={() => handleDelete(r.id)}
                        edit={() => handleEdit(r.id)}
                        optionalBtn={() => handlePermission(r.id)}
                      >
                        <td className=" py-3">{i + 1}</td>
                        <td className=" py-3">{r.id}</td>
                        <td className=" py-3">{r.name}</td>
                        <td className=" py-3">{r.code}</td>
                      </TableRow>
                    ))}
                  </tbody>
                </table>
              </div>
              <div className="row mx-2">
                <div className="col-sm-12 col-md-6">
                  <div
                    className="dataTables_info"
                    id="DataTables_Table_0_info"
                    aria-live="polite"
                  >
                    {role?.length == 0
                      ? "0 room"
                      : `Showing 1 to ${role?.length} rooms of ${totals} entries`}
                  </div>
                </div>
                <div className="col-sm-12 col-md-6 d-flex justify-content-end">
                  <PaginationBasic
                    prev={() => pageChange(page - 1)}
                    totalPage={totalPage}
                    page={page}
                    next={() => pageChange(page + 1)}
                    onPageChange={handlePageChange}
                  />
                </div>
              </div>
            </TableHeader>
          )}
        </Box>
      </Box>
    </>
  );
};

export default Role;
