import {
  reqAddRole,
  reqDeleteRole,
  reqRole,
  reqRoleById,
  reqUpdateRole,
} from "./request";
import { useDispatch, useSelector } from "react-redux";
import {
  setRole,
  setPermission,
  setTotal,
  setTotalPage,
  setParams,
} from "./reducer";
import { toast } from "react-toastify";

const useRole = () => {
  const { params } = useSelector((state) => state?.rootReducer?.role);
  const { sort } = params;
  const dispatch = useDispatch();
  const fetchRole = () => {
    reqRole(params).then((res) => {
      dispatch(setRole(res.data.data));
      dispatch(setTotal(res.data.paging.totals));
      dispatch(setTotalPage(res.data.paging.totalPage));
    });
  };
  const fetchRoleId = (id) => {
    reqRoleById(id).then((res) => {
      dispatch(setPermission(res.data.data.permissions));
    });
  };

  const addRole = (data) => {
    reqAddRole(data)
      .then(() => {
        fetchRole();
        toast.success("Role added successfully");
      })
      .catch(() => {
        toast.error("Something went wrong");
      });
  };
  const updateRole = (id, data) => {
    reqUpdateRole(id, data)
      .then(() => {
        fetchRole();
        toast.success("Role updated");
      })
      .catch(() => {
        toast.error("Something went wrong");
      });
  };
  const deleteRole = (id) => {
    reqDeleteRole(id)
      .then(() => {
        fetchRole();
        toast.success("Role deleted!");
      })
      .catch(() => {
        toast.error("Something went wrong");
      });
  };
  const updatePermission = (id, data) => {
    reqUpdateRole(id, data).then(() => {
      fetchRole();
      toast.success("Permission updated");
    });
  };
  const sortRole = (field) => {
    if (sort === field) {
      dispatch(setParams({ order: sort === field ? "desc" : "asc" }));
    } else {
      dispatch(setParams({ sort: field, order: "asc" }));
    }
  };

  const pageChange = (page) => dispatch(setParams({ page: page }));

  const limit = (limit) => {
    dispatch(setParams({ size: limit }));
  };

  const searchRole = (text) => {
    dispatch(setParams({ page: 1, query: text }));
  };
  return {
    fetchRole,
    fetchRoleId,
    updateRole,
    deleteRole,
    pageChange,
    limit,
    searchRole,
    sortRole,
    addRole,
    updatePermission,
  };
};

export { useRole };
