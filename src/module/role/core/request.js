import axios from "axios";
const reqRole = (params) => {
  return axios.get("api/roles", {
    params: params,
  });
};
const reqRoleById = (id) => {
  return axios.get(`api/roles/${id}`);
};
const reqAddRole = (data) => {
  return axios.post("api/roles", data);
};
const reqUpdateRole = (id, data) => {
  return axios.put(`api/roles/${id}`, data);
};
const reqDeleteRole = (id) => {
  return axios.delete(`api/roles/${id}`);
};
export { reqRole, reqRoleById, reqAddRole, reqDeleteRole, reqUpdateRole };
