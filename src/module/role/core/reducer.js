import { createSlice } from "@reduxjs/toolkit";

const RoleSlice = createSlice({
  name: "role",
  initialState: {
    role: [],
    permissions: [],
    addRoleModal: false,
    editRoleModal: false,
    deleteRoleModal: false,
    permissionModal: false,
    totalPage: 0,
    totals: 0,
    params: {
      size: 10,
      page: 1,
      query: "",
      sort: "",
      order: "asc",
    },
  },
  reducers: {
    setRole: (state, action) => {
      state.role = action.payload;
    },
    setPermission: (state, action) => {
      state.permissions = action.payload;
    },
    openAddRole: (state) => {
      state.addRoleModal = true;
    },
    closeAddRole: (state) => {
      state.addRoleModal = false;
    },
    openEditRole: (state) => {
      state.editRoleModal = true;
    },
    closeEditRole: (state) => {
      state.editRoleModal = false;
    },
    openDeleteRole: (state) => {
      state.deleteRoleModal = true;
    },
    closeDeleteRole: (state) => {
      state.deleteRoleModal = false;
    },
    openPermission: (state) => {
      state.permissionModal = true;
    },
    closePermission: (state) => {
      state.permissionModal = false;
    },
    setParams: (state, action) => {
      state.params = { ...state.params, ...action.payload };
    },
    setTotalPage: (state, action) => {
      state.totalPage = action.payload;
    },
    setTotal: (state, action) => {
      state.totals = action.payload;
    },
  },
});
export const {
  setRole,
  setPermission,
  openAddRole,
  closeAddRole,
  openDeleteRole,
  closeDeleteRole,
  openEditRole,
  closeEditRole,
  setParams,
  setTotalPage,
  setTotal,
  closePermission,
  openPermission,
} = RoleSlice.actions;
export default RoleSlice.reducer;
