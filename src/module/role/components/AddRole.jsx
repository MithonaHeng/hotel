import React, { useState } from "react";
import ModalForm from "../../../components/ModalForm";
import InputVert from "../../../components/InputVert";
import { useRole } from "../core/action";
import { useDispatch } from "react-redux";
import { closeAddRole } from "../core/reducer";

const AddRole = ({ show, hide }) => {
  const [roleName, setRolename] = useState("");
  const [roleCode, setRoleCode] = useState("");
  const dispatch = useDispatch();
  const data = {
    name: roleName,
    code: roleCode,
  };
  const { addRole } = useRole();
  const handleSubmit = () => {
    addRole(data);
    dispatch(closeAddRole());
  };
  return (
    <ModalForm
      header={"Add Role"}
      show={show}
      hide={hide}
      subtitle={"Add some roles"}
      submitName={"Submit"}
      submit={handleSubmit}
    >
      <InputVert
        name={"Role Name"}
        change={(e) => setRolename(e.target.value)}
      />
      <InputVert
        name={"Role Code"}
        change={(e) => setRoleCode(e.target.value)}
      />
    </ModalForm>
  );
};

export default AddRole;
