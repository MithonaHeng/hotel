import React, { useEffect, useState } from "react";
import ModalForm from "../../../components/ModalForm";
import InputVert from "../../../components/InputVert";
import { reqRoleById } from "../core/request";
import { useRole } from "../core/action";
import { useDispatch } from "react-redux";
import { closeEditRole } from "../core/reducer";

const EditRole = ({ show, hide, id }) => {
  const [data, setData] = useState({
    name: "",
    code: "",
  });
  const { updateRole } = useRole();
  const dispatch = useDispatch();

  useEffect(() => {
    reqRoleById(id).then((res) => setData(res.data.data.role));
  }, []);

  const handleSubmit = () => {
    updateRole(id, data);
    dispatch(closeEditRole());
  };
  return (
    <ModalForm
      show={show}
      header={"Edit role"}
      hide={hide}
      subtitle={"Update role"}
      submitName={"Submit"}
      submit={() => handleSubmit()}
    >
      <InputVert
        name={"Role Name"}
        value={data.name}
        change={(e) => setData({ ...data, name: e.target.value })}
      />
      <InputVert
        name={"Role Code"}
        value={data.code}
        change={(e) => setData({ ...data, code: e.target.value })}
      />
    </ModalForm>
  );
};

export default EditRole;
