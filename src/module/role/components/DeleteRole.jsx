import React from "react";
import ModalForm from "../../../components/ModalForm";
import { useRole } from "../core/action";
import { useDispatch } from "react-redux";
import { closeDeleteRole } from "../core/reducer";
const DeleteRole = ({ show, hide, id }) => {
  const { deleteRole } = useRole();
  const dispatch = useDispatch();
  const handleSubmit = () => {
    deleteRole(id);
    dispatch(closeDeleteRole());
  };
  return (
    <ModalForm
      header={"Delete Role"}
      show={show}
      hide={hide}
      subtitle={"Delete this role?"}
      submitName={"Delete"}
      submit={handleSubmit}
    ></ModalForm>
  );
};

export default DeleteRole;
