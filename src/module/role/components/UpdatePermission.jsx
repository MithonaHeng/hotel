import React, { useEffect, useState } from "react";
import ModalForm from "../../../components/ModalForm";
import { reqRoleById } from "../core/request";
import PermissionRow from "../../../components/PermissionRow";
import { useRole } from "../core/action";
import { useDispatch } from "react-redux";
import { closePermission } from "../core/reducer";

const UpdatePermission = ({ show, hide, id }) => {
  const [permission, setPermission] = useState([]);
  const { updatePermission } = useRole();
  const dispatch = useDispatch();

  const data = {
    role_id: id,
    permissions: permission.map((perm) => ({
      permissionId: perm.id,
      status: perm.status === 1 ? 1 : 0,
    })),
  };
  useEffect(() => {
    reqRoleById(id).then((res) => {
      setPermission(res.data.data.permissions);
    });
  }, []);
  const handleSubmit = (id) => {
    updatePermission(id, data);
    dispatch(closePermission());
  };
  return (
    <ModalForm
      show={show}
      hide={hide}
      header={"Edit permissions"}
      subtitle={"Update Permissions"}
      submitName={"Update"}
      submit={() => handleSubmit(id)}
    >
      <table className="table ">
        <tbody>
          {permission.map((p) => (
            <PermissionRow
              key={p.id}
              pName={p.name}
              isChecked={p.status}
              change={() => {
                const newValue = p.status === 1 ? 0 : 1;
                const newPermission = { ...p, status: newValue };
                const newValueState = permission.map((per) =>
                  per.id === p.id ? newPermission : per
                );
                setPermission(newValueState);
              }}
            />
          ))}
        </tbody>
      </table>
    </ModalForm>
  );
};

export default UpdatePermission;
