import { configureStore } from "@reduxjs/toolkit";
import usersReducer from "../usermanegement/core/userSlice.js";
import roomtype from "../roomType/core/RoomtypeSlice.js";
import { rootReducer } from "../redux/combineReducer";
import bookingReducer from "../booking/core/bookingSlice.js"
import paymentReducer from "../payment/core/paymentSlice.js"
import modalReducer from "../booking/core/modalSlice.js"
import profile from "../userProfile/core/profileSlice.js"

const store = configureStore({
  reducer: {
    users: usersReducer,
    rootReducer,
    roomtypes: roomtype,
    profile: profile,
    booking:bookingReducer,
    payment:paymentReducer,
    modal : modalReducer
  },
});

export default store;
