import { combineReducers } from "@reduxjs/toolkit";
import RoomApi from "../room/core/reducer";
import RoleSlice from "../role/core/reducer";
export const rootReducer = combineReducers({
  rooms: RoomApi,
  role: RoleSlice,
});
