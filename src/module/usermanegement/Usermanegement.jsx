import React from "react";
import Header from "../../components/Header.jsx";
import UserList from "./components/ListUser.jsx";

const UserManagement = () => {

    return (
        <div style={{margin: "0px 90px"}}>
            <Header title="USER MANAGEMENT" subtitle="List of Users"/>
            <div style={{margin: "0 0 0 0", height: "100%", width: "99%"}}>
                <UserList/>
            </div>
        </div>

    );
};

export default UserManagement;
