// UserFilter.jsx
import React from "react";

const UserFilter = ({ selectedRole, selectedStatus, handleRoleChange, handleStatusChange }) => {
  return (
    <div className="d-flex align-items-center row py-4 px-3">
      <div className="col-md-4">
        <select
          className="form-select"
          onChange={handleRoleChange}
          value={selectedRole}
        >
          <option value="">Select Role</option>
          <option value="super admin">Admin</option>
          <option value="user">User</option>
        </select>
      </div>
      <div className="col-md-4">
        <select
          className="form-select text-capitalize"
          onChange={handleStatusChange}
          value={selectedStatus}
        >
          <option value="">Select Status</option>
          <option value="Active" className="text-capitalize">
            Active
          </option>
          <option value="Inactive" className="text-capitalize">
            Inactive
          </option>
        </select>
      </div>
    </div>
  );
};

export default UserFilter;
