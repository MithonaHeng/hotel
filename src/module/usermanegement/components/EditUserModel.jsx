import React, {useEffect, useState} from "react";
import { Alert, Button, Form, Modal, ModalTitle } from "react-bootstrap";

const initialData = {
    username: "",
    name: "",
    phone: "",
    email: "",
    address: "",
    status: "",
    bio: "",
    roleId: ""
}

const EditUserModal = ({show, handleClose, userData, onSubmit}) => {
    const [updatedUser, setUpdatedUser] = useState({
        ...initialData
    });


    useEffect(() => {
        setUpdatedUser((prevData) => ({
            ...prevData,
            ...userData,
            roleId: userData?.roleEntity?.id
        }));
    }, [userData]);

    const handleChange = (e) => {
        const {name, value} = e.target;
        setUpdatedUser({...updatedUser, [name]: value});
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(updatedUser);
        setUpdatedUser({
            ...initialData
        });
    };
    return (
        <Modal show={show} onHide={handleClose} size="lg">
        <div className=" modal-content p-3 p-md-5 fw-semibold text-black">
        <Modal.Header style={{  borderBottom: 'none', justifyContent: 'center' }}>
        <ModalTitle>
          <h4 className=" d-flex justify-content-center">Edit User Information</h4>
        </ModalTitle>
      </Modal.Header>
      <Modal.Body>
        <Form>
        <div className=" row fw-semibold text-black-50">
        <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicUsername">
            <Form.Label >Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              name="username"
              value={updatedUser.username}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicName">
            <Form.Label >Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter name"
              name="name"
              value={updatedUser.name}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12  mb-3">
          <Form.Group controlId="formBasicEmail">
            <Form.Label >Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              name="email"
              value={updatedUser.email}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicPhone">
            <Form.Label >Phone</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter phone"
              name="phone"
              value={updatedUser.phone}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicAddress">
            <Form.Label >Address</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter address"
              name="address"
              value={updatedUser.address}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
          <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicStatus">
                  <Form.Label>Status</Form.Label>
                  <div className="d-flex">
                    <Form.Check
                      className=" me-3"
                      type="radio"
                      id="statusActive"
                      label="Active"
                      name="status"
                      value="1"
                      checked={updatedUser.status === "1"}
                      onChange={handleChange}
                    />
                    <Form.Check
                      className=""
                      type="radio"
                      id="statusInactive"
                      label="InActive"
                      name="status"
                      value="2"
                      checked={updatedUser.status === "2"}
                      onChange={handleChange}
                    />
                  </div>
                </Form.Group>
              </div>
          <div className="col-12 col-md-6 mb-3">
          <Form.Group controlId="formBasicRoleId">
            <Form.Label >Role</Form.Label>
            <Form.Control
              as="select"
              name="roleId"
              value={updatedUser.roleId}
              onChange={handleChange}
            >
              <option value="">Select role</option>
              <option value="1">Admin</option>
              <option value="2">User</option>
            </Form.Control>
          </Form.Group>
          </div>
          <Form.Group controlId="formBasicBio">
            <Form.Label style={{ color: "green" }}>Bio</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="Enter bio"
              name="bio"
              value={updatedUser.bio}
              onChange={handleChange}
            />
          </Form.Group>
          </div>
        </Form>
      </Modal.Body>
      <Modal.Footer style={{ borderTop: 'none', display: 'flex', justifyContent: 'center' }}>
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
    );
};

export default EditUserModal;
