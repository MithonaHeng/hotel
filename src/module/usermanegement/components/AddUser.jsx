import React, { useState } from "react";
import { Alert, Button, Form, Modal, ModalTitle } from "react-bootstrap";
import Swal from 'sweetalert2';
const AddUserModal = ({ show, handleClose,adduser}) => {
  const [userData, setUserData] = useState({
    name: "",
    username: "",
    email: "",
    phone: "",
    password: "",
    address: "",
    dateOfBirth: "",
    status: "",
    roleId: "",
    bio: "",
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserData({ ...userData, [name]: value });
  };
  const handleSubmit = () => {
    if (!userData.name || !userData.username || !userData.email || !userData.phone || !userData.password || !userData.address || !userData.dateOfBirth || !userData.status || !userData.roleId) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Please fill in all fields!',
      });
      return;
    }
    adduser(userData)
      handleClose();
      clearData();
    
  };
  const clearData = () => {
    setUserData({
      name: "",
      username: "",
      email: "",
      phone: "",
      password: "",
      address: "",
      dateOfBirth: "",
      status: "",
      roleId: "",
      bio: "",
    });
  };


  return (
    <Modal show={show} onHide={handleClose} size="lg">
      <div className=" modal-content p-3 p-md-5 fw-semibold text-black">
        <Modal.Header
          style={{ borderBottom: "none", justifyContent: "center" }}
        >
          <ModalTitle>
            <h4 className=" d-flex justify-content-center">
              Add User Information
            </h4>
            <small>Add user details will receive a privacy audit.</small>
          </ModalTitle>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <div className=" row fw-semibold text-black-50">
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicUsername">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter username"
                    name="username"
                    value={userData.username}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicName">
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter name"
                    name="name"
                    value={userData.name}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12  mb-3">
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    name="email"
                    value={userData.email}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicPhone">
                  <Form.Label>Phone</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter phone"
                    name="phone"
                    value={userData.phone}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Enter password"
                    name="password"
                    value={userData.password}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicAddress">
                  <Form.Label>Address</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter address"
                    name="address"
                    value={userData.address}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicDateOfBirth">
                  <Form.Label>Date of Birth</Form.Label>
                  <Form.Control
                    type="date"
                    placeholder="Enter date of birth"
                    name="dateOfBirth"
                    value={userData.dateOfBirth}
                    onChange={handleChange}
                  />
                </Form.Group>
              </div>
              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicStatus">
                  <Form.Label>Status</Form.Label>
                  <div className="d-flex">
                    <Form.Check
                      className=" me-3"
                      type="checkbox"
                      id="statusActive"
                      label="Active"
                      name="status"
                      value="1"
                      checked={userData.status === "1"}
                      onChange={handleChange}
                    />
                    <Form.Check
                      className=""
                      type="checkbox"
                      id="statusInactive"
                      label="Inactive"
                      name="status"
                      value="2"
                      checked={userData.status === "2"}
                      onChange={handleChange}
                    />
                  </div>
                </Form.Group>
              </div>

              <div className="col-12 col-md-6 mb-3">
                <Form.Group controlId="formBasicRoleId">
                  <Form.Label>Role</Form.Label>
                  <Form.Control
                    as="select"
                    name="roleId"
                    value={userData.roleId}
                    onChange={handleChange}
                  >
                    <option value="">Select role</option>
                    <option value="1">Admin</option>
                    <option value="2">User</option>
                  </Form.Control>
                </Form.Group>
              </div>
              <Form.Group controlId="formBasicBio">
                <Form.Label style={{ color: "green" }}>Bio</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="Enter bio"
                  name="bio"
                  value={userData.bio}
                  onChange={handleChange}
                />
              </Form.Group>
            </div>
          </Form>
        </Modal.Body>
        <Modal.Footer
          style={{
            borderTop: "none",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export default AddUserModal;
