import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useUser } from "../core/action.js";
import EditUserModal from "./EditUserModel.jsx";
import AddUserModal from "./AddUser.jsx";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import TableHeader from "../../../components/TableHeader.jsx";
import DeleteModal from "./Detele.jsx";
import UserFilter from "./UserFilter.jsx";
import UserTable from "./UserTable.jsx";
import PaginationComponent from "./Pagination.jsx";
import { useRole } from "../../role/core/action.js";
import { getRoleId } from "../../auth/components/AuthHelper";
import SortingButton from "./SortComponet.jsx";
import LoadingSpinner from "../../../components/LoadingSpinner.jsx";
const ListUser = () => {
  const {
    getUser,
    addUserData,
    editUsers,
    deleteUserID,
    handleSearch,
    handlePage,
    handleSize,
  } = useUser();
  const { users, params, totalPage, totals, loading } = useSelector(
    (state) => state.users
  );
  const [showAddUserModal, setShowAddUserModal] = useState(false);
  const [showEditUserModal, setShowEditUserModal] = useState(false);
  const [deletingUserId, setDeletingUserId] = useState(null);
  const [selectedRole, setSelectedRole] = useState("");
  const [selectedStatus, setSelectedStatus] = useState("");
  const [userData, setUserData] = useState("");
  const { fetchRoleId } = useRole();
  const roleId = getRoleId();
  const { permissions } = useSelector((state) => state?.rootReducer?.role);
  useEffect(() => {
    getUser();
    fetchRoleId(roleId);
  }, [params, deletingUserId, showAddUserModal, showEditUserModal]);

  const canCreate = permissions.some((permission) => permission.id === 6);
  const canEdit = permissions.some((permission) => permission.id === 7);
  const canDelete = permissions.some((permission) => permission.id === 8);
  const canView = permissions.some((permission) => permission.id === 5);

  const handleEditSubmit = (updatedUserData) => {
    editUsers(updatedUserData);
    setShowEditUserModal(false);
  };

  const handleEdit = (userId, userData) => {
    setUserData(userData);
    setShowEditUserModal(true);
  };

  const handleDelete = (userId) => {
    setDeletingUserId(userId);
  };

  const handleAddUser = (userData) => {
    addUserData(userData);
    setShowAddUserModal(false);
  };

  const handleRoleChange = (e) => {
    setSelectedRole(e.target.value);
  };

  const handleStatusChange = (e) => {
    setSelectedStatus(e.target.value);
  };

  const handleSizeChange = (e) => {
    handleSize(parseInt(e.target.value, 10));
  };
  const filteredUsers = users.filter((user) => {
    const roleMatch = selectedRole
      ? user.roleEntity?.code === selectedRole
      : true;
    const statusMatch = selectedStatus
      ? (user.status ? "Active" : "Inactive") === selectedStatus
      : true;
    return roleMatch && statusMatch;
  });
  const fields = [
    { field: "name", label: "NAME" },
    { field: "roleEntity", label: "ROLE" },
    { field: "createdBy", label: "CREATEBY" },
    { field: "email", label: "EMAIL" },
    { field: "phone", label: "PHONE" },
    { field: "address", label: "ADDRESS" },
    { field: "status", label: "STATUS" },
  ];

  return (
    <div className="mt-0">
      {loading ? (
        <LoadingSpinner />
      ) : !canView ? (
        <h2 className="text-center">Unauthorized</h2>
      ) : (
        <>
          <div className="bg-light rounded">
            <UserFilter
              selectedRole={selectedRole}
              selectedStatus={selectedStatus}
              handleRoleChange={handleRoleChange}
              handleStatusChange={handleStatusChange}
            />
            <div className="row mx-1 border-bottom py-3 border-top">
              <div className="col-md-6">
                <div className="col-md-2">
                  <label>
                    <select
                      className="form-select"
                      value={params.size}
                      onChange={handleSizeChange}
                    >
                      <option value="5">5</option>
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                    </select>
                  </label>
                </div>
              </div>
              <div className="col-md-6 justify-content-end d-flex">
                <div className="col-md-5">
                  <div className="d-flex align-items-center justify-content-end">
                    <label className="d-flex align-items-center">
                      Search
                      <input
                        type="search"
                        className="form-control"
                        placeholder="Search"
                        aria-label="Search"
                        onChange={(e) => handleSearch(e.target.value)}
                      />
                    </label>
                  </div>
                </div>
                <div className="col-md-2 btn-group flex-wrap">
                  {canCreate && (
                    <button
                      className="btn ms-2 btn-primary mb-3 mb-md-0"
                      onClick={() => setShowAddUserModal(true)}
                    >
                      Add
                    </button>
                  )}
                </div>
              </div>
            </div>
            <table className="table table-hover overflow-y-auto">
              <thead>
                <SortingButton fields={fields} />
              </thead>
              <UserTable
                users={filteredUsers}
                canEdit={canEdit}
                canDelete={canDelete}
                handleEdit={handleEdit}
                handleDelete={handleDelete}
              />
            </table>
            <div className="row">
              <div className="col-md-5">
                <p className="px-3 text-black fw-semibold">
                  Showing 1 to {params.size} of {totals} entries
                </p>
              </div>
              <div className="col-md-6 d-flex justify-content-end">
                <PaginationComponent
                  totalPage={totalPage}
                  params={params}
                  handlePage={handlePage}
                />
              </div>
            </div>
          </div>
          <AddUserModal
            show={showAddUserModal}
            handleClose={() => setShowAddUserModal(false)}
            addUser={handleAddUser}
          />
          <EditUserModal
            show={showEditUserModal}
            handleClose={() => setShowEditUserModal(false)}
            userData={userData}
            onSubmit={handleEditSubmit}
          />
          {canDelete && (
            <DeleteModal
              deletingUserId={deletingUserId}
              onCancel={() => setDeletingUserId(null)}
              onDelete={deleteUserID}
            />
          )}
        </>
      )}
    </div>
  );
};

export default ListUser;
