import React from "react";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { useSelector } from "react-redux";

const UserTable = ({ users, canEdit, canDelete, handleEdit, handleDelete }) => {
  const { loading } = useSelector((state) => state.users);

  if (loading) {
    return (
      <tbody>
        <tr>
          <td colSpan="8" className="text-center">
            <h4 className="text-dark">Loading...</h4>
          </td>
        </tr>
      </tbody>
    );
  }

  return (
    <tbody className="bg-light overflow-y-auto">
      {users.length === 0 ? (
        <tr>
          <td colSpan="8">
            <h4 className="text-center text-dark">User not found</h4>
          </td>
        </tr>
      ) : (
        users.map((user) => (
          <tr className="col-md-12 col-md-6" key={user.id}>
            <td>
              <div className="d-flex justify-content-start align-items-center">
                <div className="avatar-wrapper">
                  <div className="avatar avatar-sm me-1">
                    <span className="avatar-initial">
                      {user.avatar ? (
                        <img
                          key={`avatar-${user.id}`}
                          src={user.avatar}
                          alt=""
                          width="50"
                          height="50"
                          style={{
                            borderRadius: "50%",
                            marginRight: "10px",
                          }}
                        />
                      ) : (
                        <AccountCircleIcon
                          key={`avatar-placeholder-${user.id}`}
                          style={{
                            width: 50,
                            height: 50,
                            borderRadius: "50%",
                            marginRight: "10px",
                          }}
                        />
                      )}
                    </span>
                  </div>
                </div>
                <div className="d-flex flex-column">
                  <span className="fw-medium">{user.name}</span>
                  <small className="text-muted">ID: {user.id}</small>
                </div>
              </div>
            </td>
            <td className="py-3">
              <span className="text-truncate d-flex align-items-center justify-content-start">
                <span
                  style={{
                    width: 30,
                    height: 30,
                    borderRadius: "50%",
                    marginRight: "10px",
                    backgroundColor: user.roleEntity?.id === 1 ? "#e8fadf" : "#fff2d6",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  {user.roleEntity?.id === 1 ? (
                    <i className="fas fa-cog" style={{ color: "#71dd37" }}></i>
                  ) : (
                    <i className="far fa-user" style={{ color: "#ffab00" }}></i>
                  )}
                </span>
                <span>{user.roleEntity?.id === 1 ? "Admin" : "User"}</span>
              </span>
            </td>
            <td className="py-3">{user.createdBy?.name}</td>
            <td className="py-3">{user.email}</td>
            <td className="py-3">{user.phone}</td>
            <td className="py-3">{user.address}</td>
            <td className="py-3">
              <span
                className={`badge rounded-1 px-2 ${
                  user.status ? "bg-label-success" : "bg-label-warning"
                }`}
              >
                {user.status ? "ACTIVE" : "INACTIVE"}
              </span>
            </td>
            <td className="py-3">
              {canEdit && (
                <button
                  className="btn p-0 border-0 bg-transparent"
                  onClick={() => handleEdit(user.id, user)}
                >
                  <i className="fa-regular fa-pen-to-square text-primary"></i>
                </button>
              )}
              {canDelete && (
                <button className="btn border-0" onClick={() => handleDelete(user.id)}>
                  <i className="fa-solid fa-trash bg-label-danger"></i>
                </button>
              )}
            </td>
          </tr>
        ))
      )}
    </tbody>
  );
};

export default UserTable;
