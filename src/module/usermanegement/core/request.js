import axios from "axios";

const reqGetUsers = (params) => {
    return axios.get(`api/users`, {
        params: params
    });
};

const reqEditUser = (updatedUserData) => {
    return axios.put(`api/users/${updatedUserData.id}`, updatedUserData);
};
const reqAddUser = (userData) => {
    return axios.post(`api/users`, userData);
};

const reqDeleteUser = (userId) => {
    return axios.delete(`api/users/${userId}`);
};

export {
    reqGetUsers,
    reqEditUser,
    reqAddUser,
    reqDeleteUser,
};
