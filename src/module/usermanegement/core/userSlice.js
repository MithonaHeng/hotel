import {createSlice} from "@reduxjs/toolkit";

const userSlice = createSlice({
    name: "users",
    initialState: {
        users: [],
        totalPage: 0,
        loading: true,
        error: null,
        user: null,
        params: {
            size: 10,
            page: 1,
            query: "",
            sort: "name",
            order: "asc",
        }
    },
    reducers: {
        setUser: (state, action) => {
            state.users = action.payload.data;
            state.totals = action.payload.paging.totals
            state.totalPage = action.payload.paging.totalPage;
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
        setError: (state, action) => {
            state.error = action.payload;
        },
        addUser: (state, action) => {
            state.users.push(action.payload);
        },
        addUserFailure: (state, action) => {
            state.error = action.payload;
        },
        clearError(state) {
            state.error = null;
        },
        editUser: (state, action) => {
            const {userId, updatedUserData} = action.payload;
            state.users = state.users.map(user =>
                user.id === userId ? {...user, ...updatedUserData} : user
            );
        },
        deleteUser: (state, action) => {
            const userIdToDelete = action.payload;
            state.users = state.users.filter((user) => user.id !== userIdToDelete);
        },
        setUserDetail: (state, action) => {
            state.user = action.payload;
            state.loading = false;
        },
        setParams: (state, action) => {
            state.params = {...state.params, ...action.payload};
        }
    },
});

export const {
    setUser,
    setTotal,
    setLoading,
    setError,
    editUser,
    addUser,
    deleteUser,
    addUserFailure,
    clearError,
    setUserDetail,
    setParams
} = userSlice.actions;
export default userSlice.reducer;
