import {clearError, setLoading, setParams, setUser, setUserDetail} from "./userSlice.js";
import {useDispatch, useSelector} from "react-redux";
import {reqAddUser, reqDeleteUser, reqEditUser, reqGetUsers} from "./request.js";
import {toast} from "react-toastify";

const useUser = () => {
    const dispatch = useDispatch();
    const {params} = useSelector((state) => state.users);
    const getUser = () => {
        reqGetUsers(params)
          .then((res) => {
            dispatch(setUser(res.data));
            console.log(res.data);
          })
          .catch((error) => {
            console.error("Error fetching users:", error);
          })
          .finally(() => {
            dispatch(setLoading(false));
          });
      };

    const addUserData = (userData) => {
        if (!validateUserData(userData)) {
            return;
        }
        reqAddUser(userData)
            .then((res) => { 
                toast.success(res.data.message);
                getUser();
            })
            .catch((error) => {
                toast.error("Error: " + error.response.data.message);
                clearErrors();
            });
    };
    const handleSort = (field) => {
        const newOrder = params.order === 'asc' ? 'desc' : 'asc';
        dispatch(setParams({ sort: field, order: newOrder }));
    };
    
    const editUsers = (updatedUserData) => {
        if (!validateUserData(updatedUserData)) {
            return;
        }
         reqEditUser(updatedUserData)
            .then((res) => {
                toast.success(res.data.message);
                getUser();
                clearErrors();
            })
            .catch((error) => {
                toast.error("Error: " + error.response.data.message);
            });
    };
    const deleteUserID = (userId) => {
        reqDeleteUser(userId)
            .then((res) => {
                toast.success(res.data.message);
                getUser();
            })
            .catch((error) => {
                toast.error("Error: " + error.response.data.message);
            });
    };
    const clearErrors = () => {
        dispatch(clearError());
    };

    const handlePage = (page) => dispatch(setParams({page: page}))

    const handleSize = (size) => {
        dispatch(setParams({size:size}));
      };
    
    const handleSearch = (text) => {
        dispatch(setParams({page: 1, query: text}));
    };
    const validateUserData = (userData) => {
        if (!isValidEmail(userData.email)) {
            toast.error("Please enter a valid email address again!");
            return false;
        }
        if (!isValidPhoneNumber(userData.phone)) {
            toast.error("Please enter a valid phone number again!");
            return false;
        }
        return true;
    };
    const isValidEmail = (email) => {
       
        const emailRegex = /^[^\s@]+@gmail\.com$/;
        return emailRegex.test(email);
    };
    
    const isValidPhoneNumber = (phone) => {
        const phoneRegex = /^0\d{9}$/;
        return phoneRegex.test(phone);
    };

    return {
        getUser,
        editUsers,
        addUserData,
        deleteUserID,
        clearErrors,
        handlePage,
        handleSort,
        handleSearch,
        handleSize,
    };
};

export {useUser};