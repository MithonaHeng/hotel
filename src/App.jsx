import { useState } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import Topbar from "./module/admin/components/global/Topbar";
import Sidebar from "./module/admin/components/global/Sidebar";
import Dashboard from "./module/admin/components/dashboard";
import Team from "./module/admin/components/team";
import Bar from "./module/admin/components/bar";
import Line from "./module/admin/components/line";
import Pie from "./module/admin/components/pie";
import FAQ from "./module/admin/components/faq";
import Geography from "./module/admin/components/geography";
import Report from "./module/admin/components/report/report";
import Usermanegement from "./module/usermanegement/Usermanegement.jsx";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "./theme";
import Calendar from "./module/admin/components/calendar/calendar";
import Login from "./module/auth/Login";
import Register from "./module/auth/Register";
import { useAuth } from "./module/auth/components/Auth";
import RoomType from "./module/roomType/RoomType.jsx";
import { ToastContainer } from "react-toastify";
import Role from "./module/role/Role";
import Room from "./module/room/Room.jsx";
import Booking from "./module/booking/Booking.jsx";
import BookingDetail from "./module/booking/components/BookingDetail.jsx";
import Payment from "./module/payment/Payment.jsx";
import RoomTypeDetail from "./module/roomType/components/RoomTypeDetail.jsx";

function App() {
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const { auth } = useAuth();

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          {auth ? (
            <>
              <Sidebar isSidebar={isSidebar} />
              <main className="content">
                <Topbar setIsSidebar={setIsSidebar} />
                <Routes>
                  <Route path="*" element={<Navigate to="/" />} />
                  <Route path="/" element={<Dashboard />} />
                  <Route path="/team" element={<Team />} />
                  <Route path="/booking" element={<Booking />} />
                  <Route path="/booking/:id" element={<BookingDetail />} />
                  <Route path="/payment" element={<Payment />} />
                  <Route path="/payment/:id" element={<Payment />} />
                  <Route path="/role" element={<Role />} />
                  <Route path="/room" element={<Room />} />
                  <Route path="/roomtype" element={<RoomType />} />
                  <Route path="/roomtypedetail/:id" element={<RoomTypeDetail />} />
                  <Route path="/usermanegement" element={<Usermanegement />} />
                  <Route path="/bar" element={<Bar />} />
                  <Route path="/pie" element={<Pie />} />
                  <Route path="/line" element={<Line />} />
                  <Route path="/faq" element={<FAQ />} />
                  <Route path="/calendar" element={<Calendar />} />
                  <Route path="/geography" element={<Geography />} />
                </Routes>
              </main>
            </>
          ) : (
            <Routes>
              <Route index path="/auth/login" element={<Login />} />
              <Route path="*" element={<Navigate to="/auth/login" />} />
              <Route path="/register" element={<Register />} />
            </Routes>
          )}
          <ToastContainer position="top-right" />
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;
